package com.goldengate.extentReportListner;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.goldengate.pageTests.LoginTest;

public class ScreenshotListner implements ITestListener {
	
	static String destFile;
	static String finaldestination;
	 // This method will execute before starting of Test suite.
	 public void onStart(ITestContext tr) {

	 }

	 // This method will execute, Once the Test suite is finished.
	 public void onFinish(ITestContext tr) {

	 }

	 // This method will execute only when the test is pass.
	 public void onTestSuccess(ITestResult tr) {
	  captureScreenShot(tr, "pass");
	  
	 }

	 // This method will execute only on the event of fail test.
	 public void onTestFailure(ITestResult tr) {
	  captureScreenShot(tr, "fail");
	 }

	 // This method will execute before the main test start (@Test)
	 public void onTestStart(ITestResult tr) {

	 }

	 // This method will execute only if any of the main test(@Test) get skipped
	 public void onTestSkipped(ITestResult tr) {
	 }

	 public void onTestFailedButWithinSuccessPercentage(ITestResult tr) {
	 }

	 // Function to capture screenshot.
	 public void captureScreenShot(ITestResult result, String status) {
	  // AndroidDriver driver=ScreenshotOnPassFail.getDriver();
	  String destDir = "";
	  String destDirForReport = "";

	  String passfailMethod = result.getMethod().getRealClass().getSimpleName() + "." + result.getMethod().getMethodName();
	  // To capture screenshot.
	  File scrFile = ((TakesScreenshot) LoginTest.driver).getScreenshotAs(OutputType.FILE);
	  DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
	  // If status = fail then set folder name "screenshots/Failures"
	  if (status.equalsIgnoreCase("fail")) {
	   destDir = "test-output/screenshots/Failures";
	   destDirForReport = "screenshots/Failures";
	  }
	  // If status = pass then set folder name "screenshots/Success"
	  else if (status.equalsIgnoreCase("pass")) {
	   destDir = "test-output/screenshots/Success";
	   destDirForReport = "screenshots/Success";
	  }

	  // To create folder to store screenshots
	  new File(destDir).mkdirs();
	  // Set file name with combination of test class name + date time.
	  destFile = passfailMethod + " - " + dateFormat.format(new Date()) + ".png";

	  try {
	   // Store file at destination folder location
	   FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
	   finaldestination = destDirForReport + "/" + destFile;
	  } catch (IOException e) {
	   e.printStackTrace();
	  }
	  result.setAttribute("screenshotPath", finaldestination);
	 }
	 
	 public static String getScreenshotPath() {
		 System.out.println(ScreenshotListner.destFile);
		 System.out.println(ScreenshotListner.finaldestination);

		 return finaldestination;
	 }
	}