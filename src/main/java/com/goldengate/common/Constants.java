package com.goldengate.common;

public class Constants {

	public static String excelSheetName = "LoginData.xlsx";
	public static String expected_invalid_login_message = "Please enter valid Username and Password.";
	public static String expected_mobile_validation = "Please enter a valid mobile number";
	public static String expected_mobile_validation_wrong_number = "Please enter a valid mobile number";
	public static String expected_mobile_validation_no_number = "Please enter a 10 digit mobile number";
	public static String expected_password_validation = "Please enter your Paytm password";
	public static String ShopOwnerValidation = "Please enter Name of Shop Owner";
	public static String ShopNameValidation = "Please enter Name of Shop";
	public static String ShopNumberValidation = "Please enter Shop No. / Floor / Building ";
	public static String ShopStreetValidation = "Please enter Street name";
	public static String AreaValidation = "Please enter Area / Sector / Village";
	public static String StateValidation = "Please add  State ";
	public static String CityValidation = "Please add  City / Town / District ";
	public static String PinValidation = "Please enter PIN Code";
	public static String ShopFrontPhotoValidation = "Please add Shop Front Photo";
	public static String QRValidation = "Please add QR Sticker Photo";
	public static String PaytmAcceptValidation = "Please add Paytm Accepted Here Sticker Photo";
	public static String invalid_otp_message = "Invalid OTP entered. Please try again with correct OTP.";
	public static String usedOtpMessage = "This OTP is already used. Please try again with correct OTP.";
	public static String expected_business_owner_name_validation = "Please Enter Business Owner Name";
	public static String expected_business_owner_name_regex = "Please Enter valid Business Owner Name";
	public static String expected_business_name_validation = "Please Enter Business Name";
	public static String expected_business_name_regex = "Please Enter valid Business Name";
	public static String expected_pan_regex = "Please enter valid PAN";
	public static String expected_doing_business_as_validation = "Please Enter Doing Business As";
	public static String expected_house_no_validation = "Please enter House No./ Floor/ Building";
	public static String expected_house_no_no_input_validation = "Please enter House No./ Floor/ Building";
	public static String expected_house_no_wrong_input_validation = "Please enter valid House No./ Floor/ Building";
	public static String expected_area_validation = "Please Enter Area/ Locality/ Sector";
	public static String expected_area_no_input_validation = "Please Enter Area/ Locality/ Sector";
	public static String expected_street_name_wrong_input_validation = "Please Enter valid street";
	public static String expected_area_wrong_input_validation = "Please Enter valid Area/ Locality/ Sector";
	public static String expected_pincode_validation = "Please Enter PIN Code";
	public static String expected_pincode_invalid_message = "Invalid PIN Code";
	public static String expected_pincode_failed_fetch_message = "Failed to fetch pincode details.";
	public static String expected_house_no_regex = "Please enter valid House No./ Floor/ Building";
	public static String expected_street_name_regex = "Please Enter valid street";
	public static String expected_area_regex = "Please Enter valid Area/ Locality/ Sector";
	public static String expected_pincode_regex = "Invalid PIN Code";
	public static String expected_alternate_no_regex = "Please Enter Valid Mobile no";
	public static String expected_gstn_optional_regex = "Please Enter Valid GSTIN";
	public static String expected_city_validation = "Please Enter City";
	public static String expected_language_validation = "Please Select Language";
	public static String expected_bankAccount_validation = "Please Enter Bank Account Number";
	public static String expected_bankAccount_regex = "Bank Account Number should not less than 6 digits";
	public static String expected_ifscCode_validation = "Please enter valid IFSC Code";
	public static String expected_ifscCode_regex = "Please Enter Valid IFSC Code";
	public static String expected_invalid_pincode_validation = "Failed to fetch pincode details.";
	public static String expected_qr_sticker_validation = "Please click QR Sticker Code Photo";
	public static String expected_paytm_sticker_validation = "Please click Paytm Accepted Here Sticker Photo";
	public static String expected_shop_photo_validation = "Please click Shop Front Photo";
	public static String expected_cancel_cheque_validation = "Please click Cancel Cheque Photo";
	public static String expected_find_ifsc_validation = "Please enter valid IFSC Code";
	public static String expected_category_validation_message = "Please Select Category";
	public static String expected_sub_category_validation_message = "Please Select Sub Category";
	public static String expected_display_name_validation_message = "Enter Display Name / Brand Name";
	public static String expected_business_details_no_selection = "You cannot proceed without selecting business type.";
	public static String expected_business_details_no_proprietor_selection = "Please proceed with Business Profile creation using Proprietor's mobile number.";
	public static String expected_business_valid_pan_validation = "Please enter valid PAN";
	public static String expected_business_valid_pan_popup_validation = "Please enter a valid PAN";
	public static String expected_submit_document_no_selection = "Please select one value";
	public static String expected_submit_document_no_photo = "Please click";
	public static String expected_opening_time_validation = "Please Select Opening Time";
	public static String expected_closing_time_validation = "Please Select Closing Time";
	public static String expected_email_validation = "Please Select Closing Time";

	


	
	

	public static String expected_business_pan_used_pan = "Entered PAN is already linked to an account. Please enter correct PAN.";
	
	
	


}
