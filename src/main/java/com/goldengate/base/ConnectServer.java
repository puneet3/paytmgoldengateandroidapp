package com.goldengate.base;

import java.io.IOException;
import java.io.InputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class ConnectServer {
	
	public static JSch jsch = new JSch();
	public static Session session;
	public static Channel channel;

	public static void connectToServer(String PGProxyHost, String PGProxyUser, String PGProxyPassword) throws JSchException {
		java.util.Properties config = new java.util.Properties(); 
		config.put("StrictHostKeyChecking", "no");
		session = jsch.getSession(PGProxyUser, PGProxyHost, 22);
		session.setPassword(PGProxyPassword);
		session.setConfig(config);
		session.connect();
		System.out.println("session connect");
	}
	
	public static String getOtp(String phoneNumber) throws JSchException, IOException {
		String otp = null;
		String command = "grep "+phoneNumber+" /paytm/logs/pgproxy-notification.log | grep -oP '(?<!\\d)\\d{6}(?!\\d)' | tail -n 1";
		channel = session.openChannel("exec");
		((ChannelExec)channel).setCommand(command);

		channel.setInputStream(null);
		((ChannelExec)channel).setErrStream(System.err);
		InputStream in = channel.getInputStream();
		channel.connect();

		byte[] tmp = new byte[1024];
		while (true) {
		  while (in.available() > 0) {
		      int i = in.read(tmp, 0, 1024);
		      if (i < 0) {
		          break;
		      }
		     // System.out.print(new String(tmp, 0, i));
		  }
		  if (channel.isClosed()) {
		      if (channel.getExitStatus() == 0) {
		    	  otp = "";
		    	  otp = new String(tmp);
		      }
		      break;
		  }
		}
		return otp;

	}
	
	public static void closeConnection() {
		channel.disconnect();
		session.disconnect();
		
	}
}
	
