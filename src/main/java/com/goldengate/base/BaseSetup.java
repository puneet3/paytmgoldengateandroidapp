package com.goldengate.base;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.goldengate.pageObjects.PageObjects.LoginLocators;

import io.appium.java_client.android.AndroidDriver;

public class BaseSetup {
	public static AndroidDriver<WebElement> driver;
	DesiredCapabilities capabilities = new DesiredCapabilities();
	public static final Logger log = Logger.getLogger(BaseSetup.class);

	@BeforeSuite(alwaysRun = true)
	public void setCapabilities() throws Throwable  {
		log.info("Setting Capabilities");
		capabilities.setCapability("noReset", "true");
		capabilities.setCapability("deviceName", "0123456789ABCDEF");
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "Android");
		capabilities.setCapability(CapabilityType.VERSION, "5.1");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("app", System.getProperty("user.dir")+"/src/test/resources/GG_v3.5.7_staging_1.apk");
		capabilities.setCapability("appPackage", "com.paytm.goldengate.debug");
		capabilities.setCapability("appActivity", "com.paytm.goldengate.main.activities.SplashActivity");
		
		log.info("Staring Driver/GoldenGate App");
		driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		//driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
	}
	
	
	
	@AfterSuite(alwaysRun = true)
	public void closeDriver() {
		log.info("Closing Driver/GoldenGate App");
		driver.quit();
	}
}
   