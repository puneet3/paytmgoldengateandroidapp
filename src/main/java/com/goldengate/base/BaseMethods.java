package com.goldengate.base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;

import com.goldengate.common.Constants;
import com.goldengate.excelReader.ExcelReader;
import com.goldengate.pageObjects.PageObjects;
import com.jcraft.jsch.JSchException;

import io.appium.java_client.android.AndroidKeyCode;

public class BaseMethods extends BaseSetup {

	public static final Logger log = Logger.getLogger(BaseMethods.class);
	ConnectServer connectServer = new ConnectServer();
	public static String PGProxyHost = "10.144.18.47";
	public static String PGProxyUser = "pgtest";
	public static String PGProxyPassword = "pgtest@123";
	Properties prop = new Properties();

	public Properties propLoad() {

		try {

			String FilePath = System.getProperty("user.dir") + "/src/main/java/com/goldengate/config/or.properties";
			FileInputStream Locator = new FileInputStream(FilePath);
			prop.load(Locator);
			return prop;

		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return null;
	}

	public ExcelReader getTestData() {

		try {

			String currentFilePath = System.getProperty("user.dir") + "/src/main/resources/TestcaseData";
			log.info("initializined the xlsx file from location" + currentFilePath);
			ExcelReader readData = new ExcelReader(currentFilePath, Constants.excelSheetName);
			log.info("Initialization Ended");
			return readData;

		} catch (Exception e) {
			// TODO: handle exception
			log.info("exception during initializing the xls file " + e);
		}
		return null;
	}

	public void typeInput(By byLocator, String locatorValue) {
		//driver.findElement(byLocator).clear();
		driver.findElement(byLocator).sendKeys(locatorValue);
		log.info("Value Entered in Locator : " + byLocator + " with value : " + locatorValue);
	}

	public void clearTextbox(By byLocator) {
		driver.findElement(byLocator).clear();

	}

	public void clickObject(By byLocator) {
		driver.findElement(byLocator).click();
		log.info("Locator CLicked : " + byLocator);

	}

	public void captureImage() {
		driver.pressKeyCode(27);
		clickObject(PageObjects.CameraCommonLocators.IMAGE_CLICK_DONE);

	}

	public void dismissKeypad() {
		driver.hideKeyboard();
		log.info("Keypad Dismissed");
	}

	public boolean isElememtPresent(By byLocator) {
		int trialCount = 1;
		while (trialCount <= 1) {
			try {
				if (driver.findElement(byLocator).isDisplayed()) {
					log.info("Element : " + byLocator + " found at trial count : " + trialCount);
					return true;
				}
			} catch (NoSuchElementException e) {
				log.info("Element : " + byLocator + " not found at trial count : " + trialCount);
				trialCount++;
			}
		}
		return false;
	}

	public void navigateBackOneStep() {
		driver.pressKeyCode(AndroidKeyCode.BACK);
		log.info("Navigating Back One Step");
	}

	public String getText(By byLocator) {
		String visibleText = driver.findElement(byLocator).getText();
		log.info("Text visibe at locator" + byLocator + "is" + visibleText);
		return visibleText;
	}

	public void navigateBackToHome() {
		if(isElememtPresent(PageObjects.ErrorPopupLocators.ERROR_POPUP_OK_BUTTON)) {
			clickObject(PageObjects.ErrorPopupLocators.ERROR_POPUP_OK_BUTTON);
		}
		int status = 0;
		while (status == 0) {
			if (isElememtPresent(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS) != true) {
				log.info("Navigating Back One Step");
				driver.pressKeyCode(AndroidKeyCode.BACK);
			} else {
				status = 1;
				log.info("Navigated Back to Home");
			}
		}
	}

	public void login(String loginMobileNumber, String loginPassword) {

		if (isElememtPresent(PageObjects.LoginLocators.PAYTM_LOGO)) {

			log.info("Logged In to App");
			typeInput(PageObjects.LoginLocators.LOGIN_MOBILE_NUMBER, loginMobileNumber);
			typeInput(PageObjects.LoginLocators.LOGIN_PASSWORD, loginPassword);
			dismissKeypad();
			clickObject(PageObjects.LoginLocators.LOGIN_BUTTON);
		}
		return;
	}


	public void signout() {
		log.info("Signing Out");
		navigateBackToHome();
		driver.findElement(PageObjects.HomeScreenLocators.HAMBURGER_MENU_ICON).click();
		driver.findElement(PageObjects.HomeScreenMenuLocators.SIGN_OUT_LINK).click();
	}

	public void waitForLoad(long miliSeconds) {
		log.info("Explicit Wait Started");
		try {
			synchronized (this) {
				this.wait(miliSeconds);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		log.info("Explicit Wait End");
	}

	public void verticalScroll(double paramA, double paramB, By byLocator) {
		log.info("Scrolling Vertically");
		int x =1;
		while (x<=3) {
			try {
				if (driver.findElement(byLocator).isDisplayed()) {
					x = 4;
				}

			} catch (NoSuchElementException e) {
				Dimension appWindowSize = driver.manage().window().getSize();
				int startX = (appWindowSize.width / 2);
				int startY = (int) (appWindowSize.height * paramA);
				int endX = startX;
				int endY = (int) (appWindowSize.height * paramB);
				driver.swipe(startX, startY, endX, endY, 3000);
				x++;

			}
		}
	}

	public void verticalScroll(double paramA, double paramB) {

		Dimension appWindowSize = driver.manage().window().getSize();
		int startX = (appWindowSize.width / 2);
		int startY = (int) (appWindowSize.height * paramA);
		int endX = startX;
		int endY = (int) (appWindowSize.height * paramB);
		driver.swipe(startX, startY, endX, endY, 3000);

	}

	public void allowPermission(By byLocator) {
		int x = 1;
		while (x<=10) {
			try {
				if (driver.findElement(byLocator).isDisplayed()) {
					driver.findElement(byLocator).click();
					log.info("Accepting Grant Request");
				}
				
				
			} catch (NoSuchElementException e) {
				x++;
				log.info("No More Requests to be Granted");
			}
		}
	}

	public boolean isElementClickable(By byLoactor) {
		return driver.findElement(byLoactor).isEnabled();

	}

	public String getOTP(String phoneNumber) throws JSchException, IOException {
		String otp;
		ConnectServer.connectToServer(PGProxyHost, PGProxyUser, PGProxyPassword);
		otp = ConnectServer.getOtp(phoneNumber);
		ConnectServer.closeConnection();
		if (otp != "") {
			log.info("OTP is successfully generated and fetched.");
			return otp;
		} else {
			log.info("OTP fetched as NULL");
			return null;
		}

	}

	public void inputOTP(String otpValue) {
		char[] ch = otpValue.toCharArray();
		for (int i = 0; i < 6; i++) {
			driver.pressKeyCode(keyCodeMapping(ch[i]));
		}
	}

	public int keyCodeMapping(char digitKey) {
		int keypadCodeKey = 0;
		switch (digitKey) {
		case '0':
			keypadCodeKey = AndroidKeyCode.KEYCODE_0;
			break;
		case '1':
			keypadCodeKey = AndroidKeyCode.KEYCODE_1;
			break;
		case '2':
			keypadCodeKey = AndroidKeyCode.KEYCODE_2;
			break;
		case '3':
			keypadCodeKey = AndroidKeyCode.KEYCODE_3;
			break;
		case '4':
			keypadCodeKey = AndroidKeyCode.KEYCODE_4;
			break;
		case '5':
			keypadCodeKey = AndroidKeyCode.KEYCODE_5;
			break;
		case '6':
			keypadCodeKey = AndroidKeyCode.KEYCODE_6;
			break;
		case '7':
			keypadCodeKey = AndroidKeyCode.KEYCODE_7;
			break;
		case '8':
			keypadCodeKey = AndroidKeyCode.KEYCODE_8;
			break;
		case '9':
			keypadCodeKey = AndroidKeyCode.KEYCODE_9;
			break;
		}
		return keypadCodeKey;
	}

	public void setRadioButtonValue(String radioButtonLocator, String radioButtonValue) {
		By dynamicLocator = null;
		String temp = radioButtonLocator + radioButtonValue + "']";
		System.out.println("Dynamic path Created : " + temp);
		dynamicLocator = By.xpath(temp);
		verticalScroll(0.8, 0.2, dynamicLocator);
		clickObject(dynamicLocator);
	}
	
	public void waitForLoad(By destinationLocator) {
		int tryCount = 0;
		while (tryCount < 5) {
			try {
				if(isElememtPresent(destinationLocator)) {
					return;
				}
				else {
					waitForLoad(2000);
					tryCount++;
				}
			} catch (Exception e) {
				waitForLoad(2000);
				tryCount++;
			}
		}
	}
}
