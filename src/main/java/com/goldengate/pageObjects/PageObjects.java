package com.goldengate.pageObjects;

import org.openqa.selenium.By;

public class PageObjects {
	public interface LoginLocators {

		// Locators for Login Screen
		final By LOGIN_MOBILE_NUMBER = By.id("com.paytm.goldengate.debug:id/fragment_login_et_mobile");
		final By LOGIN_PASSWORD = By.id("com.paytm.goldengate.debug:id/fragment_login_et_password");
		final By LOGIN_BUTTON = By.id("com.paytm.goldengate.debug:id/button_login");
		final By INVALID_LOGIN_MESSAGE = By
				.xpath("//android.widget.TextView[@text ='Please enter valid Username and Password.']");
		final By MOBILE_VALIDATION_MESSAGE = By.xpath(
				"//android.widget.TextView[contains(@resource-id,'com.paytm.goldengate.debug:id/textinput_error') and @text ='Please enter a valid mobile number']");
		final By PASSWORD_VALIDATION_MESSAGE = By.xpath(
				"//android.widget.TextView[contains(@resource-id,'com.paytm.goldengate.debug:id/textinput_error') and @text ='Please enter your Paytm password']");
		final By PAYTM_LOGO = By.id("com.paytm.goldengate.debug:id/paytm_logo");
		final By LOGIN_HEADER = By.id("com.paytm.goldengate.debug:id/txt_merchant_header1");
	}

	public interface HomeScreenLocators {
		// Locators for Home Screen
		final By INDIVIDUAL_OFFERINGS = By.xpath("//android.widget.TextView[@text ='Individual\n" + "Offerings']");
		final By BUSINESS_OFFERINGS = By.xpath("//android.widget.TextView[@text ='Business\n" + "Offerings']");
		final By MY_WORK = By.id("com.paytm.goldengate.debug:id/fragment_tv_my_task");
		final By HAMBURGER_MENU_ICON = By.className("android.widget.ImageButton");
	}

	public interface IndividualOfferingsLocators {
		final By QR_MERCHANT_LINK = By.xpath("//android.widget.TextView[@text='QR Merchant']");
		final By INDIVIDUAL_CURRENT_ACCOUNT = By.xpath("//android.widget.TextView[@text='Individual Current Account']");
		final By BUSINESS_CORRESPONDENT_LINK = By.xpath("//android.widget.TextView[@text='Business Correspondent']");

	}

	public interface HomeScreenMenuLocators {
		final By SIGN_OUT_LINK = By.id("com.paytm.goldengate.debug:id/footer_nav_signout");
	}

	public interface PermissionPopupLocators {
		final By ALLOW_PERMISSION_POPUP_MESSAGE = By.xpath(
				"//android.widget.Button[contains(@resource-id,'com.android.packageinstaller:id/permission_allow_button') and @text='Allow']");
		// "//android.widget.Button[@text,'ALLOW']"); //replaced the previous locator
		// with index
		final By ALLOW_PERMISSION_POPUP_MSG = By.xpath(
				"//android.widget.Button[contains(@resource-id,'com.android.packageinstaller:id/permission_allow_button') and @text='ALLOW']");

	}

	public interface IndividualOnboardingCommonLocators {
		final By CUSTOMER_MOBILE_NUMBER = By.id("com.paytm.goldengate.debug:id/fragment_kyc_et_mobile_number");
		final By PROCEED_BUTTON = By.id("com.paytm.goldengate.debug:id/fragment_mobile_number_proceed_button_new");
		final By TC_AGREE_BUTTON = By.id("com.paytm.goldengate.debug:id/agreed_btn");
		final By VALIDATE_OTP_PROCEED_BUTTON = By.id("com.paytm.goldengate.debug:id/button_submit");
		final By LEAD_CREATD_LINK = By.xpath("//android.widget.TextView[@text='Status : Lead Created']");
		final By TnC_TITLE = By.xpath("//android.widget.TextView[@text='Terms & Conditions']");
		final By OTP_TITLE = By.id("com.paytm.goldengate.debug:id/text_heading_otp_sent");
	}

	public interface CameraCommonLocators {
		final By IMAGE_CLICK_DONE = By.xpath("//android.widget.ImageView[contains(@resource-id,'btn_done')]");
	}

	public interface BusinessOfferingsCommonLocators {
		final By BACK_BUTTON = By.className("android.widget.ImageButton");
		
		final By APPLICANT_MOBILE_NUMBER_TITLE = By.id("com.paytm.goldengate.debug:id/kyc_heading");
		final By APPLICANT_MOBILE_NUMBER_MOBILE_NUMBER = By.id("com.paytm.goldengate.debug:id/fragment_kyc_et_mobile_number");
		final By APPLICANT_MOBILE_NUMBER_PROCEED_BUTTON = By.id("com.paytm.goldengate.debug:id/fragment_mobile_number_proceed_button_new");
		final By APPLICANT_MOBILE_NUMBER_MOBILE_VALIDATION_MESSAGE = By.id("com.paytm.goldengate.debug:id/textinput_error");
		
		final By OTP_TITLE = By.id("com.paytm.goldengate.debug:id/text_heading_otp_sent");
		final By OTP_RESEND_CODE = By.id("com.paytm.goldengate.debug:id/fragment_otp_resend");
		final By OTP_PROCEED_BUTTON = By.id("com.paytm.goldengate.debug:id/button_submit");
		
		final By PROVIDE_BUSINESS_DETAILS_TITLE = By.id("com.paytm.goldengate.debug:id/pan_header_tv");
		final By PROVIDE_BUSINESS_DETAILS_PROPRIETORSHIP_RADIO_BUTTON = By.xpath("//android.widget.RadioButton[@text='Proprietorship']");
		final By PROVIDE_BUSINESS_DETAILS_PUBLIC_LIMITED_RADIO_BUTTON = By.xpath("//android.widget.RadioButton[@text='Public Limited']");
		final By PROVIDE_BUSINESS_DETAILS_PRIVATE_LIMITED_RADIO_BUTTON = By.xpath("//android.widget.RadioButton[@text='Private Limited']");
		final By PROVIDE_BUSINESS_DETAILS_PARTNERSHIP_RADIO_BUTTON = By.xpath("//android.widget.RadioButton[@text='Partnership']");
		final By PROVIDE_BUSINESS_DETAILS_HUF_RADIO_BUTTON = By.xpath("//android.widget.RadioButton[@text='HUF']");
		final By PROVIDE_BUSINESS_DETAILS_SOCIETY_RADIO_BUTTON = By.xpath("//android.widget.RadioButton[@text='Society']");
		final By PROVIDE_BUSINESS_DETAILS_TRUST_RADIO_BUTTON = By.xpath("//android.widget.RadioButton[@text='Trust']");
		final By PROVIDE_BUSINESS_DETAILS_INDIVIDUAL_RADIO_BUTTON = By.xpath("//android.widget.RadioButton[@text='Individual']");
		final By PROVIDE_BUSINESS_DETAILS_PROPRIETOR_YES_RADIO_BUTTON = By.xpath("//android.widget.RadioButton[@text='Yes']");
		final By PROVIDE_BUSINESS_DETAILS_PROPRIETOR_NO_RADIO_BUTTON = By.xpath("//android.widget.RadioButton[@text='No']");
		final By PROVIDE_BUSINESS_DETAILS_BUSINESS_PAN = By.id("com.paytm.goldengate.debug:id/fragment_kyc_et_pan_number");
		final By PROVIDE_BUSINESS_DETAILS_RADIO_BUTTON_ERROR = By.id("com.paytm.goldengate.debug:id/error_textview_radiobutton");
		final By PROVIDE_BUSINESS_DETAILS_BUSINESS_PAN_ERROR = By.id("com.paytm.goldengate.debug:id/textinput_error");

		final By PROVIDE_BUSINESS_DETAILS_PROCEED_BUTTON = By.id("com.paytm.goldengate.debug:id/fragment_mobile_number_proceed_button_new");
		
		final By CONFIRM_BUSINESS_DETAILS_TITLE = By.id("com.paytm.goldengate.debug:id/tvMessage");
		final By CONFIRM_BUSINESS_DETAILS_BUSINESS_NAME = By.id("com.paytm.goldengate.debug:id/et_business_name");
		final By CONFIRM_BUSINESS_DETAILS_CLOSE_BUTTON = By.id("com.paytm.goldengate.debug:id/iv_close");
		final By CONFIRM_BUSINESS_DETAILS_CONFIRM_BUTTON = By.id("com.paytm.goldengate.debug:id/button_proceed");
		
		final By BUSINESS_PROFILE_TITLE = By.xpath("//android.widget.TextView[@text='Business Profile']");
		final By BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS = By
				.id("com.paytm.goldengate.debug:id/fragment_company_onboarding_profile_status_tv");
		
		final By BUSINESS_DETAILS_TITLE = By.id("com.paytm.goldengate.debug:id/business_details_tv");
		final By BUSINESS_DETAILS_BUSINESS_TYPE = By.id("com.paytm.goldengate.debug:id/edit_ca_entity_type");
		final By BUSINESS_DETAILS_BUSINESS_PAN = By.id("com.paytm.goldengate.debug:id/edit_ca_pan");
		final By BUSINESS_DETAILS_BUSINESS_CATEGORY = By.id("com.paytm.goldengate.debug:id/spinnerCategory");
		final By BUSINESS_DETAILS_BUSINESS_SUB_CATEGORY = By.id("com.paytm.goldengate.debug:id/spinnerSubCategory");
		final By BUSINESS_DETAILS_REGISTERED_BUSINESS_NAME = By
				.id("com.paytm.goldengate.debug:id/edit_ca_business_name");
		final By BUSINESS_DETAILS_DISPLAY_NAME = By.id("com.paytm.goldengate.debug:id/edit_ca_business_display_name");
		final By BUSINESS_DETAILS_COUNTRY_OF_INCORPORATION = By.id("com.paytm.goldengate.debug:id/spinner_country");
		final String BUSINESS_DETAILS_SELECT_DROPDOWN_VALUE = "//android.widget.CheckedTextView[@text='";
		final By BUSINESS_DETAILS_BUSINESS_CATEGGORY_ERROR = By
				.id("com.paytm.goldengate.debug:id/error_textview_spinner_cat");
		final By BUSINESS_DETAILS_BUSINESS_SUB_CATEGOTY_ERROR = By
				.id("com.paytm.goldengate.debug:id/error_textview_spinner_sub_cat");
		final By BUSINESS_DETAILS_DISPLAY_NAME_ERROR = By.id("com.paytm.goldengate.debug:id/textinput_error");
		final By BUSINESS_DETAILS_PROCEED_BUTTON = By.id("com.paytm.goldengate.debug:id/btn_banking_confirm_ca");
		
		final By ADDRESS_TITLE = By.id("");
		final By ADDRESS_HOUSE_NUMBER = By.id("com.paytm.goldengate.debug:id/fragment_shop_address_et");
		final By ADDRESS_HOUSE_NUMBER_ERROR = By.xpath("//android.widget.LinearLayout[@index='1']/*/"
				+ "android.widget.TextView[@resource-id='com.paytm.goldengate.debug:id/textinput_error']");
		final By ADDRESS_STREET_NAME = By.id("com.paytm.goldengate.debug:id/fragment_street_et");
		final By ADDRESS_STREET_NAME_ERROR = By.xpath("//android.widget.LinearLayout[@index='2']/*/"
				+ "android.widget.TextView[@resource-id='com.paytm.goldengate.debug:id/textinput_error']");
		final By ADDRESS_AREA = By.id("com.paytm.goldengate.debug:id/fragment_area_sector_et");
		final By ADDRESS_AREA_ERROR = By.xpath("//android.widget.LinearLayout[@index='3']/*/"
				+ "android.widget.TextView[@resource-id='com.paytm.goldengate.debug:id/textinput_error']");
		final By ADDRESS_PIN_CODE = By.id("com.paytm.goldengate.debug:id/fragment_pincode_et");
		final By ADDRESS_PIN_CODE_ERROR = By.xpath("//android.widget.LinearLayout[@index='4']/*/"
				+ "android.widget.TextView[@resource-id='com.paytm.goldengate.debug:id/textinput_error']");
		final By ADDRESS_STATE = By.id("com.paytm.goldengate.debug:id/fragment_state_et");
		final By ADDRESS_CITY = By.id("com.paytm.goldengate.debug:id/fragment_city_et");
		final By ADDRESS_NEXT_BUTTON = By.id("com.paytm.goldengate.debug:id/fragment_address_btn_next");
		
		final By ADDITIONAL_DETAILS_GSTIN = By.id("com.paytm.goldengate.debug:id/fragment_gstin_et");
		final By ADDITIONAL_DETAILS_GSTIN_ERROR = By
				.xpath("//android.widget.TextView[@text='Please Enter Valid GSTIN']");
		final By ADDITIONAL_DETAILS_NEXT_BUTTON = By
				.id("com.paytm.goldengate.debug:id/fragment_addtional_details_btn_next");
		
		final By SUBMIT_BUSINESS_DOCUMENT_PHOTO_PLACE_BUSINESS_LATER = By.xpath(
				"//android.widget.LinearLayout[@index='0']/*/" + "android.widget.RadioButton[@text='Provide Later']");
		final By SUBMIT_BUSINESS_DOCUMENT_PHOTO_PLACE_BUSINESS_NOW = By.xpath(
				"//android.widget.LinearLayout[@index='0']/*/" + "android.widget.RadioButton[@text='Capture Now']");
		final By SUBMIT_BUSINESS_DOCUMENT_PHOTO_PLACE_BUSINESS_ERROR_NO_SELECTION = By
				.xpath("//android.widget.LinearLayout[@index='0']/" + "android.widget.TextView[@index='2']");
		final By SUBMIT_BUSINESS_DOCUMENT_PHOTO_PLACE_BUSINESS_CAMERA = By
				.xpath("//android.widget.LinearLayout[@index='1']/*/" + "android.widget.RelativeLayout[@index='0']");
		final By SUBMIT_BUSINESS_DOCUMENT_PHOTO_PLACE_BUSINESS_ERROR_NO_PHOTO = By
				.xpath("//android.widget.LinearLayout[@index='1']/*/*/" + "android.widget.TextView[@index='1']");
		final By SUBMIT_BUSINESS_DOCUMENT_ACTIVITY_PROOF_1_LATER = By
				.xpath("//android.widget.LinearLayout[@index='15']/*/*/*/"
						+ "android.widget.RadioButton[@text='Provide Later']");
		final By SUBMIT_BUSINESS_DOCUMENT_ACTIVITY_PROOF_1_NOW = By
				.xpath("//android.widget.LinearLayout[@index='15']/*/*/*/"
						+ "android.widget.RadioButton[@text='Capture Now']");
		final By SUBMIT_BUSINESS_DOCUMENT_ACTIVITY_PROOF_1_ERROR_NO_SELECTION = By
				.xpath("//android.widget.LinearLayout[@index='15']/*/*/" + "android.widget.TextView[@index='2']");
		final By SUBMIT_BUSINESS_DOCUMENT_ACTIVITTY_PROOF_1_DROPDOWN = By.xpath(
				"//android.widget.LinearLayout[@index='16']/*/*/*/*/" + "android.widget.TextView[@text='Select']");
		final String SUBMIT_BUSINESS_DOCUMENT_SELECT_DROPDOWN_VALUE = "//android.widget.CheckedTextView[@text='";
		final By SUBMIT_BUSINESS_DOCUMENT_ACTIVITTY_PROOF_1_DROPDOWN_ERROR = By
				.xpath("//android.widget.LinearLayout[@index='16']/*/*/*/" + "android.widget.TextView[@index='3']");
		final By SUBMIT_BUSINESS_DOCUMENT_ACTIVITY_PROOF_1_CAMERA = By
				.xpath("//android.widget.LinearLayout[@index='17']/*/" + "android.widget.RelativeLayout[@index='0']");
		final By SUBMIT_BUSINESS_DOCUMENT_ACTIVITY_PROOF_1_ERROR_NO_PHOTO = By
				.xpath("//android.widget.LinearLayout[@index='17']/*/*/*/" + "android.widget.TextView[@index='1']");
		final By SUBMIT_BUSINESS_DOCUMENT_SUBMIT_BUTTON = By
				.id("com.paytm.goldengate.debug:id/fragment_image_capture_btn_next");

		final By LIST_OF_DOCUMENTS_TO_UPLOAD_PROCEED_BUTTON = By
				.id("com.paytm.goldengate.debug:id/button_place_kyc_request");
	}

	public interface QrMerchant100kLocators {
		final By ACCEPT_PAYMENT_IN_BANK_ACCOUNT = By
				.xpath("//android.widget.TextView[@text='Accept Payments in Bank Account']");
		final By CATEGORY = By.xpath(
				"//android.widget.LinearLayout[@index ='0']/android.widget.LinearLayout[@index ='2']/android.widget.Spinner[@index ='1']");

		final By AUTOMOBILES_AND_VEHICLES = By
				.xpath("//android.widget.CheckedTextView[@text='Automobiles and Vehicles']");

		final By SUBCATEGORY = By.xpath(
				"//android.widget.LinearLayout[@index ='0']/android.widget.LinearLayout[@index ='3']/android.widget.Spinner[@index ='1']");

		final By AUTHORIZED_SERVICE_CENTRE = By
				.xpath("//android.widget.CheckedTextView[@text='Authorized Service Centre']");

		final By NAME_OF_BUSINESS_OWNER = By.id("com.paytm.goldengate.debug:id/fragment_name_of_shop_owner_et");
		final By NAME_OF_BUSINESS = By.id("com.paytm.goldengate.debug:id/fragment_name_of_shop_et");
		final By PAN_OPTIONAL = By.id("com.paytm.goldengate.debug:id/fragment_pan_et");
		final By PAN_OPTIONAL_VALIDATION = By
				.xpath("//TextInputLayout[@index ='5']/*/android.widget.TextView[@index='0']");

		final By BASIC_DETAILS_NEXT_BUTTON = By.id("com.paytm.goldengate.debug:id/fragment_btn_next");

		final By DOING_BUSINESS_AS = By.id("com.paytm.goldengate.debug:id/fragment_shop_name_et");
		final By HOUSE_NO = By.id("com.paytm.goldengate.debug:id/fragment_shop_address_et");
		final By STREET_NAME = By.id("com.paytm.goldengate.debug:id/fragment_street_et");
		final By AREA = By.id("com.paytm.goldengate.debug:id/fragment_area_sector_et");
		final By PINCODE = By.id("com.paytm.goldengate.debug:id/fragment_pincode_et");
		final By CITY_TOWN_DISTRICT = By.id("com.paytm.goldengate.debug:id/fragment_city_et");
		final By ADDRESS_DETAILS_NEXT_BUTTON = By.id("com.paytm.goldengate.debug:id/fragment_address_btn_next");

		final By ALTERNATE_NUMBER_OPTIONAL = By.id("com.paytm.goldengate.debug:id/fragment_alternate_no_et");
		final By LANGUAGE_PREFERENCES = By.xpath(
				"//android.widget.LinearLayout[@index ='4']/android.widget.LinearLayout[@index ='0']/android.widget.LinearLayout[@index ='0']/android.widget.Spinner[@index ='1']");
		final By HINDI_LANGUAGE = By.xpath("//android.widget.CheckedTextView[@text= 'Hindi']");

		final By GSTN_OPTIONAL = By.id("com.paytm.goldengate.debug:id/fragment_gstin_et");
		final By ADDITIONAL_DETAILS_NEXT_BUTTON = By
				.id("com.paytm.goldengate.debug:id/fragment_addtional_details_btn_next");

		final By BANK_ACCOUNT_NO = By.id("com.paytm.goldengate.debug:id/edit_p2b_bank_ac_no");
		final By IFSC_CODE = By.id("com.paytm.goldengate.debug:id/edit_p2b_ifsc_code");
		final By CONFIRM_BUTTON = By.id("com.paytm.goldengate.debug:id/btn_banking_confirm_p2p_100k");
		final By FIND_IFSC = By.id("com.paytm.goldengate.debug:id/tv_find_ifsc");
		final By SELECT_BANK = By.id("com.paytm.goldengate.debug:id/txt_select_bank");
		final By SEARCH_BAR = By.id("com.paytm.goldengate.debug:id/search_src_text");
		final By SELECT_BANK_FROM_LIST = By.xpath("//android.widget.TextView[@text='STATE BANK OF INDIA']");
		final By SELECT_STATE_FROM_LIST = By.xpath("//android.widget.TextView[@text='UTTAR PRADESH']");
		final By SELECT_CITY_FROM_LIST = By.xpath("//android.widget.TextView[@text='NOIDA']");
		final By SELECT_BRANCH_FROM_LIST = By.xpath("//android.widget.TextView[@text='NOIDA']");

		final By SELECT_STATE = By.id("com.paytm.goldengate.debug:id/txt_select_state");
		final By SELECT_CITY = By.id("com.paytm.goldengate.debug:id/txt_select_city");
		final By SELECT_BRANCH = By.id("com.paytm.goldengate.debug:id/txt_select_branch");
		final By IFSC_CONTINUE_BUTTON = By.id("com.paytm.goldengate.debug:id/btn_continue");
		final By QR_STICKER1 = By
				.xpath("//android.widget.LinearLayout[@index ='4']/*/android.widget.RelativeLayout[@index ='0']");
		final By QR_STICKER2_OPTIONAL = By
				.xpath("//android.widget.LinearLayout[@index ='5']/*/android.widget.RelativeLayout[@index ='0']");
		final By QR_STICKER3_OPTIONAL = By
				.xpath("//android.widget.LinearLayout[@index ='6']/*/android.widget.RelativeLayout[@index ='0']");
		final By PAYTM_ACCEPTED_HERE1 = By
				.xpath("//android.widget.LinearLayout[@index ='7']/*/android.widget.RelativeLayout[@index ='0']");
		final By PAYTM_ACCEPTED_HERE2_OPTIONAL = By
				.xpath("//android.widget.LinearLayout[@index ='8']/*/android.widget.RelativeLayout[@index ='0']");
		final By PAYTM_ACCEPTED_HERE3_OPTIONAL = By
				.xpath("//android.widget.LinearLayout[@index ='9']/*/android.widget.RelativeLayout[@index ='0']");
		final By SHOP_FRONT_PHOTO = By
				.xpath("//android.widget.LinearLayout[@index ='10']/*/android.widget.RelativeLayout[@index ='0']");
		final By CANCEL_CHEQUE_PHOTO = By
				.xpath("//android.widget.LinearLayout[@index ='22']/*/android.widget.RelativeLayout[@index ='0']");
		final By LEAD_SUBMIT_BUTTON = By.id("com.paytm.goldengate.debug:id/fragment_images_form_btn_next");
		final By NAME_OF_BUSINESS_OWNER_VALIDATION = By
				.xpath("//TextInputLayout[@index ='1']/*/android.widget.TextView[@index ='0']");
		final By NAME_OF_BUSINESS_OWNER_REGEX = By.xpath(
				"//TextInputLayout[@index ='1']/android.widget.FrameLayout[@index ='0']/*/android.widget.TextView[@index ='0']");
		final By NAME_OF_BUSINESS_VALIDATION = By
				.xpath("//TextInputLayout[@index ='4']/*/android.widget.TextView[@index ='0']");
		final By NAME_OF_BUSINESS_REGEX = By
				.xpath("//TextInputLayout[@index ='4']/*/android.widget.TextView[@index ='0']");
		final By PAN_OPTIONAL_REGEX = By.xpath("//TextInputLayout[@index ='5']/*/android.widget.TextView[@index ='0']");

		final By DOING_BUINESS_AS_VALIDATION = By
				.xpath("//TextInputLayout[@index ='0']/*/android.widget.TextView[@index='0']");
		final By HOUSE_NO_VALIDATION = By.xpath("//TextInputLayout[@index ='1']/*/android.widget.TextView[@index='0']");
		final By AREA_VALIDATION = By.xpath("//TextInputLayout[@index ='3']/*/android.widget.TextView[@index='0']");
		final By PINCODE_VALIDATION = By.xpath("//TextInputLayout[@index ='4']/*/android.widget.TextView[@index='0']");
		final By HOUSE_NO_REGEX = By.xpath("//TextInputLayout[@index ='1']/*/android.widget.TextView[@index='0']");
		final By STREET_NAME_REGEX = By.xpath("//TextInputLayout[@index ='2']/*/android.widget.TextView[@index='0']");
		final By AREA_REGEX = By.xpath("//TextInputLayout[@index ='3']/*/android.widget.TextView[@index='0']");
		final By PINCODE_REGEX = By.xpath("//TextInputLayout[@index ='4']/*/android.widget.TextView[@index='0']");

	}

	public interface BusinessCorrespondentLocators {
		final By BCA_TYPE = By.id("com.paytm.goldengate.debug:id/widget_dynamic_layout_spinner");
		final By BCA_FIXED_TYPE = By.xpath("//android.widget.CheckedTextView[@text='Fixed']");
		final By BCA_MOBILE_TYPE = By.xpath("//android.widget.CheckedTextView[@text='Mobile']");
		final By BCA_SUBTYPE = By.id("com.paytm.goldengate.debug:id/spinner_bca_subType");

		final By BCA_SUB_TYPE_TELECOM = By.xpath("//android.widget.CheckedTextView[@text='Telecom']");

		final By NAME_OF_BUSINESS_OWNER = By.id("com.paytm.goldengate.debug:id/fragment_name_of_shop_owner_et");
		final By NAME_OF_BUSINESS = By.id("com.paytm.goldengate.debug:id/fragment_name_of_shop_et");
		final By PAN_OPTIONAL = By.id("com.paytm.goldengate.debug:id/fragment_pan_et");
		final By PINCODE = By.id("com.paytm.goldengate.debug:id/fragment_pincode_et");
		final By BASIC_DETAILS_NEXT_BUTTON = By.id("com.paytm.goldengate.debug:id/fragment_btn_next");

		final By DOING_BUSINESS_AS = By.id("com.paytm.goldengate.debug:id/fragment_shop_name_et");
		final By HOUSE_NO = By.id("com.paytm.goldengate.debug:id/fragment_shop_address_et");
		final By STREET_NAME = By.id("com.paytm.goldengate.debug:id/fragment_street_et");
		final By AREA = By.id("com.paytm.goldengate.debug:id/fragment_area_sector_et");
		final By CITY_TOWN_DISTRICT = By.id("com.paytm.goldengate.debug:id/fragment_city_et");
		final By ADDRESS_DETAILS_NEXT_BUTTON = By.id("com.paytm.goldengate.debug:id/fragment_address_btn_next");

		final By ALTERNATE_NUMBER_OPTIONAL = By.id("com.paytm.goldengate.debug:id/fragment_alternate_no_et");
		final By LANGUAGE_PREFERENCES = By.xpath(
				"//android.widget.LinearLayout[@index ='4']/android.widget.LinearLayout[@index ='0']/android.widget.LinearLayout[@index ='0']/android.widget.Spinner[@index ='1']");

		final By LANGUAGE_ENGLISH = By.xpath("//android.widget.CheckedTextView[@text='English']");

		final By OPENING_TIME = By.id("com.paytm.goldengate.debug:id/spinnerOpeningTime");

		final By SHOP_OPENING_TIME = By.xpath("//android.widget.CheckedTextView[@text='9 AM']");

		final By CLOSING_TIME = By.id("com.paytm.goldengate.debug:id/spinnerClosingTime");
		final By SHOP_CLOSING_TIME = By.xpath("//android.widget.CheckedTextView[@text='9 PM']");

		final By EMAIL_ID = By.id("com.paytm.goldengate.debug:id/fragment_email_et");
		final By GSTN_OPTIONAL = By.id("com.paytm.goldengate.debug:id/fragment_gstin_et");
		final By ADDITIONAL_DETAILS_NEXT_BUTTON = By
				.id("com.paytm.goldengate.debug:id/fragment_addtional_details_btn_next");
		final By NO_OF_YEARS_IN_BUSINESS = By
				.xpath("//android.widget.RadioButton[@text='Greater than or equal to 3 years']");
		final By HOW_WIDE_STORE = By.xpath("//android.widget.RadioButton[@text='Greater than or equal to 10 feet']");
		final By AVERAGE_DAILY_CUSTOMER_WALKINS = By
				.xpath("//android.widget.RadioButton[@text='Greater than or equal to 100']");
		final By DISTANCE_NEAREST_BRANCH = By.xpath("//android.widget.RadioButton[@text='Greater than 10 kms']");
		final By TOTAL_NUMBER_OF_STAFF = By.xpath("//android.widget.RadioButton[@text= 'Greater than 1']");
		final By EDUCATIONL_QUALIFICATION = By.xpath("//android.widget.RadioButton[@text= 'Graduate and above']");
		final By DO_KYC_OF_ANOTHER_BANK = By.xpath("//android.widget.RadioButton[@text= 'Graduate and above']");
		final By AGE_OF_MANAGER = By.xpath("//android.widget.RadioButton[@text='Between 20-50']");
		final By HAVING_NETBANKING = By.xpath("//android.widget.RadioButton[@text='Yes']");
		final By DECLARATION_CHECKBOX = By.id("com.paytm.goldengate.debug:id/tv_checkList");
		final By DECLARATION_NEXT_BUTTON = By.id("com.paytm.goldengate.debug:id/fragment_merchant_btn_next");
		final By PRODUCT_POSTER_PHOTO = By
				.xpath("android.widget.LinearLayout[@index = 1]/*/android.widget.RelativeLayout[@index=0]");
		final By COMPLIANCE_POSTER_PHOTO = By
				.xpath("android.widget.LinearLayout[@index = 2]/*/android.widget.RelativeLayout[@index=0]");
		final By PPB_STICKER_PHOTO = By
				.xpath("android.widget.LinearLayout[@index = 3]/*/android.widget.RelativeLayout[@index=0]");
		final By SHOP_FRONT_PHOTO = By
				.xpath("android.widget.LinearLayout[@index = 4]/*/android.widget.RelativeLayout[@index=0]");
		final By LEAD_SUBMIT_BUTTON = By.id("com.paytm.goldengate.debug:id/fragment_images_form_btn_next");

		final By BCA_TYPE_VALIDATION = By
				.xpath("android.widget.LinearLayout[@index = 0]/*/android.widget.TextView[@index=3]");
		final By BCA_SUBTYPE_VALIDATION = By
				.xpath("android.widget.LinearLayout[@index = 3]/*/android.widget.TextView[@index=3]");

		final By NAME_OF_BUSINESS_OWNER_VALIDATION = By
				.xpath("//TextInputLayout[@index ='1']/*/android.widget.TextView[@index ='0']");
		final By NAME_OF_BUSINESS_OWNER_REGEX = By.xpath(
				"//TextInputLayout[@index ='1']/android.widget.FrameLayout[@index ='0']/*/android.widget.TextView[@index ='0']");
		final By NAME_OF_BUSINESS_VALIDATION = By
				.xpath("//TextInputLayout[@index ='4']/*/android.widget.TextView[@index ='0']");
		final By NAME_OF_BUSINESS_REGEX = By
				.xpath("//TextInputLayout[@index ='4']/*/android.widget.TextView[@index ='0']");
		final By PAN_OPTIONAL_REGEX = By.xpath("//TextInputLayout[@index ='5']/*/android.widget.TextView[@index ='0']");
		final By PAN_OPTIONAL_VALIDATION = By
				.xpath("//TextInputLayout[@index ='5']/*/android.widget.TextView[@index='0']");

		final By DOING_BUINESS_AS_VALIDATION = By
				.xpath("//TextInputLayout[@index ='0']/*/android.widget.TextView[@index='0']");
		final By HOUSE_NO_VALIDATION = By.xpath("//TextInputLayout[@index ='1']/*/android.widget.TextView[@index='0']");
		final By AREA_VALIDATION = By.xpath("//TextInputLayout[@index ='3']/*/android.widget.TextView[@index='0']");
		final By PINCODE_VALIDATION = By.xpath("//TextInputLayout[@index ='4']/*/android.widget.TextView[@index='0']");
		final By HOUSE_NO_REGEX = By.xpath("//TextInputLayout[@index ='1']/*/android.widget.TextView[@index='0']");
		final By STREET_NAME_REGEX = By.xpath("//TextInputLayout[@index ='2']/*/android.widget.TextView[@index='0']");
		final By AREA_REGEX = By.xpath("//TextInputLayout[@index ='3']/*/android.widget.TextView[@index='0']");
		final By PINCODE_REGEX = By.xpath("//TextInputLayout[@index ='4']/*/android.widget.TextView[@index='0']");
		final By ALTERNATE_NUMBER_OPTIONAL_REGEX = By
				.xpath("//TextInputLayout[@index ='0']/*/android.widget.TextView[@index='0']");
		final By LANGUAGE_PREFERENCES_VALIDATION = By
				.xpath("android.widget.LinearLayout[@index = 4]/*/android.widget.TextView[@index=3]");
		final By OPENING_TIME_VALIDATION = By
				.xpath("android.widget.LinearLayout[@index = 5]/*/android.widget.TextView[@index=3]");
		final By CLOSING_TIME_VALIDATION = By
				.xpath("android.widget.LinearLayout[@index = 6]/*/android.widget.TextView[@index=3]");
		final By EMAIL_ID_VALIDATION = By
				.xpath("//TextInputLayout[@index ='7']/*/android.widget.TextView[@index='0']");
		final By GSTN_OPTIONAL_VALIDATION =  By
				.xpath("//TextInputLayout[@index ='8']/*/android.widget.TextView[@index='0']");

	}

	public interface IndividualCurrentAccountLocators {
		final By CUSTOMER_MOBILE_NUMBER = By.id("com.paytm.goldengate.debug:id/enter_mobile_number_edittext");
		final By MOBILE_VALIDATION_MESSAGE = By.id("com.paytm.goldengate.debug:id/textinput_error");
		final By PROCEED_BUTTON = By.id("com.paytm.goldengate.debug:id/enter_mobile_number_proceed");
		final String OTP_VALUE = "com.paytm.goldengate.debug:id/et_value_";
	}

	public interface ErrorPopupLocators {
		final By ERROR_POPUP_MESSAGE = By.xpath("//android.widget.TextView[@resource-id='android:id/message']");
		final By ERROR_POPUP_OK_BUTTON = By.xpath("//android.widget.Button[@text='OK']");
	}
}
