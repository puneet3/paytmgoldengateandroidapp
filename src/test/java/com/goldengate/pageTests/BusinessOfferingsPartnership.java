package com.goldengate.pageTests;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.goldengate.base.BaseMethods;
import com.goldengate.common.Constants;
import com.goldengate.pageObjects.PageObjects;
import com.jcraft.jsch.JSchException;

import junit.framework.Assert;

public class BusinessOfferingsPartnership extends BaseMethods {
	//added new code
	// Test Data --  Give details of a KYCed Applicant
	int leadStatus = 0; // 0 means first time lead created, 1 means lead already created.
	public static String customerMobileNumber = "6123451181";
	public static String otp = null;
	public static String businessPAN = "AAAFA1001A";
//	public static String businessName = "PG BN Automation1";
	public static String businessDisplayName = "PG DN Regression b";
	public static String category = "BFSI";
	public static String subCategory = "Loans";
//	public static String addressHouseNumber = "12345";
//	public static String addressStreetName = "PG SN Automation";
//	public static String addressArea = "PG A Automation";
//	public static String addressPinCode = "201301";
//	public static String gstin = "";
//	public static String activityProofType = "GST certificate";

	@Test(priority = 0, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testValidNavigation() {
		waitForLoad(5000);
		login("6123451111", "agent123");
		waitForLoad(5000);
		if(navigateCustomerMobileScreen()) {
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_TITLE));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 3, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testValidMobileSubmit() {
		if(submitMobileNumber()) {
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.OTP_TITLE));	
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}

	@Test(priority = 4, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testValidOTPSubmit() throws JSchException, IOException {
		if(submitValidOTP()) {
			if(leadStatus==0) {
				log.info("First Time Lead Creation");
				//leadStatus = 1;
				waitForLoad(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_TITLE);
				Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_TITLE));
			}else if(leadStatus==1) {
				log.info("Lead was already created");
				waitForLoad(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS);
				Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS));
			}
				
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 5, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testNoBusinessSolutionSelectionValidation() throws JSchException, IOException {
		if (leadStatus==1) throw new SkipException("Lead was already created");
		if(submitValidOTP()) {
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_TITLE);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PROCEED_BUTTON);
			Assert.assertEquals(Constants.expected_business_details_no_selection,
					getText(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_RADIO_BUTTON_ERROR));
		}
	}
	
	@Test(priority = 6, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testNoPANValidation() throws JSchException, IOException {
		if (leadStatus==1) throw new SkipException("Lead was already created");
		if(submitPartnershipSolution("")) {
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_BUSINESS_PAN_ERROR);
			String actualErrorMessage = getText(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_BUSINESS_PAN_ERROR);
			Assert.assertTrue(actualErrorMessage.contains(Constants.expected_business_valid_pan_validation));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}	

	
	@Test(priority = 7, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testInvalidPANValidation() throws JSchException, IOException {
		if (leadStatus==1) throw new SkipException("Lead was already created");
		if(submitPartnershipSolution("AUXPG8711F")) {
			waitForLoad(PageObjects.ErrorPopupLocators.ERROR_POPUP_OK_BUTTON);
			String actualErrorPopupMessage = getText(PageObjects.ErrorPopupLocators.ERROR_POPUP_MESSAGE);
			Assert.assertTrue(actualErrorPopupMessage.contains(Constants.expected_business_valid_pan_popup_validation));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}	

	@Test(priority = 8, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testInCompletePANValidation() throws JSchException, IOException {
		if (leadStatus==1) throw new SkipException("Lead was already created");
		if(submitPartnershipSolution("AAAFA")) {
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_BUSINESS_PAN_ERROR);
			String actualErrorMessage = getText(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_BUSINESS_PAN_ERROR);
			Assert.assertTrue(actualErrorMessage.contains(Constants.expected_business_valid_pan_validation));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}	

	@Test(priority = 9, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testUsedPANValidation() throws JSchException, IOException {
		if (leadStatus==1) throw new SkipException("Lead was already created");
		if(submitPartnershipSolution("AAAFA1002A")) {
			waitForLoad(PageObjects.ErrorPopupLocators.ERROR_POPUP_OK_BUTTON);
			String actualErrorPopupMessage = getText(PageObjects.ErrorPopupLocators.ERROR_POPUP_MESSAGE);
			Assert.assertTrue(actualErrorPopupMessage.contains(Constants.expected_business_pan_used_pan));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}	
	
	@Test(priority = 10, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testValidSubmitPartnershipSolution() throws JSchException, IOException {
		if (leadStatus==1) throw new SkipException("Lead was already created");
		if(submitPartnershipSolution(businessPAN)) {
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.CONFIRM_BUSINESS_DETAILS_TITLE);
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.CONFIRM_BUSINESS_DETAILS_TITLE));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 11, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testCancelConfirmBusinessDetails() throws JSchException, IOException {
		if (leadStatus==1) throw new SkipException("Lead was already created");
		if(submitPartnershipSolution(businessPAN)) {
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.CONFIRM_BUSINESS_DETAILS_TITLE);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.CONFIRM_BUSINESS_DETAILS_CLOSE_BUTTON);
			verticalScroll(0.2, 0.8);
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_TITLE));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 12, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testSubmitConfirmBusinessDetails() throws JSchException, IOException {
		if (leadStatus==1) throw new SkipException("Lead was already created");
		if(submitConfirmBusinessDetails()) {
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_TITLE);
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_TITLE));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 13, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testBusinessProfileLeadStatusfromBO() throws JSchException, IOException {
		if(businessProfileStatusfromBO()) {
			//waitForLoad(5000);
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_TITLE);
			Assert.assertEquals(getText(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS), " Lead Created");
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}

	@Test(priority = 14, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testSelectPartnershipOnboardLead() throws JSchException, IOException {
		if(selectPartnershipOnboardLead(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS)) {
			//waitForLoad(5000);
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_TITLE);
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_TITLE));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 15, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testCategoryValidation() throws JSchException, IOException {
		if(selectPartnershipOnboardLead(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS)) {
			waitForLoad(5000);
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_PROCEED_BUTTON);
			Assert.assertEquals(Constants.expected_category_validation_message, 
					getText(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_CATEGGORY_ERROR));			
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 16, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testBusinessDetailsSubCategoryValidation() throws JSchException, IOException {
		if(selectPartnershipOnboardLead(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS)) {
			waitForLoad(5000);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_CATEGORY);
			setRadioButtonValue(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_SELECT_DROPDOWN_VALUE, category);
			waitForLoad(3000);
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_PROCEED_BUTTON);
			Assert.assertEquals(Constants.expected_sub_category_validation_message, 
					getText(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_SUB_CATEGOTY_ERROR));			
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}	

	@Test(priority = 17, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testBusinessDetailsDisplayNameValidation() throws JSchException, IOException {
		if(selectPartnershipOnboardLead(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS)) {
			waitForLoad(5000);
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_PROCEED_BUTTON);
			Assert.assertEquals(Constants.expected_display_name_validation_message, 
					getText(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_DISPLAY_NAME_ERROR));			
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}

	@Test(priority = 18, enabled = true, groups = {"BusinessOfferingsPartnership"})
	public void testSubmitValidBusinessDetails() throws JSchException, IOException {
		if(submitValidBusinessDetails()) {
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_HOUSE_NUMBER));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}

	// Helper Functions | Test Steps
	public boolean navigateCustomerMobileScreen() {
		//login("6123451111","agent123");
		//waitForLoad(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
		waitForLoad(5000);
		navigateBackToHome();
		clickObject(PageObjects.HomeScreenLocators.BUSINESS_OFFERINGS);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_TITLE);
		//waitForLoad(2000);
		return true;
	}
	
	public boolean submitMobileNumber() {
		navigateCustomerMobileScreen();
		//waitForLoad(5000);
		typeInput(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_MOBILE_NUMBER, customerMobileNumber);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_PROCEED_BUTTON);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.OTP_TITLE);
		//waitForLoad(5000);
		return true;
	}
	
	public boolean submitValidOTP() throws JSchException, IOException {
		submitMobileNumber();
		//waitForLoad(5000);
		waitForLoad(3000);
		otp = getOTP(customerMobileNumber);
		inputOTP(otp);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.OTP_PROCEED_BUTTON);
		//waitForLoad(7000);
		return true;
	}
	
	public boolean submitPartnershipSolution(String businessPANValue) throws JSchException, IOException {
		submitValidOTP();
		//waitForLoad(5000);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_TITLE);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PARTNERSHIP_RADIO_BUTTON);
		verticalScroll(0.8, 0.2);
		typeInput(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_BUSINESS_PAN, businessPANValue);
		dismissKeypad();
		verticalScroll(0.8, 0.2);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PROCEED_BUTTON);
		return true;
	}
	
	public boolean submitConfirmBusinessDetails() throws JSchException, IOException {
		submitPartnershipSolution(businessPAN);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.CONFIRM_BUSINESS_DETAILS_TITLE);;
		clickObject(PageObjects.BusinessOfferingsCommonLocators.CONFIRM_BUSINESS_DETAILS_CONFIRM_BUTTON);
		return true;
	}
	
	public boolean businessProfileStatusfromBO() throws JSchException, IOException {
		submitValidOTP();
		return true;
	}
	
	public boolean selectPartnershipOnboardLead(By businessSolutionLocator) throws JSchException, IOException {
		businessProfileStatusfromBO();
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_TITLE);
		clickObject(businessSolutionLocator);
		return true;
	}

	public boolean submitValidBusinessDetails() throws JSchException, IOException {
		selectPartnershipOnboardLead(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS);
		waitForLoad(5000);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_CATEGORY);
		setRadioButtonValue(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_SELECT_DROPDOWN_VALUE, category);
		waitForLoad(3000);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_SUB_CATEGORY);
		setRadioButtonValue(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_SELECT_DROPDOWN_VALUE, subCategory);
		waitForLoad(3000);
		verticalScroll(0.8, 0.2);
		typeInput(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_DISPLAY_NAME, businessDisplayName);
		dismissKeypad();
		verticalScroll(0.8, 0.2);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_PROCEED_BUTTON);
		return true;
	}

}
