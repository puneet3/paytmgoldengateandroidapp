package com.goldengate.pageTests;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.testng.annotations.Test;

import com.goldengate.base.BaseMethods;
import com.goldengate.common.Constants;
import com.goldengate.pageObjects.PageObjects;
import com.jcraft.jsch.JSchException;

public class IndividualCurrentAccount extends BaseMethods{
	
	public static final Logger log = Logger.getLogger(IndividualCurrentAccount.class);
	
	@Test(priority = 0, enabled = true, groups = {"IndividualCurrentAccount"})
	public void testSolutionNavigation() throws JSchException, IOException {
		log.info("Inside  testSolutionNavigation() test.....");
		login("6123451111","agent123");
		waitForLoad(15000);
		clickObject(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
		verticalScroll(0.8, 0.2);
		Assert.assertTrue(isElementClickable(PageObjects.IndividualOfferingsLocators.INDIVIDUAL_CURRENT_ACCOUNT));
	}

	@Test(priority = 1, enabled = true, groups = {"IndividualCurrentAccount"})
	public void testMobileValidationNoNumber() {
		log.info("Inside  testMobileValidationNoNumber() test.....");
//		login("6123451111","agent123");
//		clickObject(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
//		verticalScroll(0.8, 0.2);
		clickObject(PageObjects.IndividualOfferingsLocators.INDIVIDUAL_CURRENT_ACCOUNT);
		typeInput(PageObjects.IndividualCurrentAccountLocators.CUSTOMER_MOBILE_NUMBER, "");
		clickObject(PageObjects.IndividualCurrentAccountLocators.PROCEED_BUTTON);
		Assert.assertEquals(getText(PageObjects.IndividualCurrentAccountLocators.MOBILE_VALIDATION_MESSAGE),
				Constants.expected_mobile_validation_no_number);	
	}
	
	@Test(priority = 2, enabled = false, groups = {"IndividualCurrentAccount"})
	public void testMobileValidationWrongNumber() {
		log.info("Inside  testMobileValidationWrongNumber() test.....");
//		login("6123451111","agent123");
//		clickObject(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
//		verticalScroll(0.8, 0.2);
//		clickObject(PageObjects.IndividualOfferingsLocators.INDIVIDUAL_CURRENT_ACCOUNT);
		typeInput(PageObjects.IndividualCurrentAccountLocators.CUSTOMER_MOBILE_NUMBER, "1234");
		Assert.assertEquals(getText(PageObjects.IndividualCurrentAccountLocators.MOBILE_VALIDATION_MESSAGE),
				Constants.expected_mobile_validation_wrong_number);	
	}
		
	@Test(priority = 3, enabled = true, groups = {"IndividualCurrentAccount"})
	public void testTnC() throws JSchException, IOException {
		log.info("Inside  testTnC() test.....");
//		login("6123451111","agent123");
//		clickObject(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
//		verticalScroll(0.8, 0.2);
//		clickObject(PageObjects.IndividualOfferingsLocators.INDIVIDUAL_CURRENT_ACCOUNT);
		typeInput(PageObjects.IndividualCurrentAccountLocators.CUSTOMER_MOBILE_NUMBER, "6123451131");
		clickObject(PageObjects.IndividualCurrentAccountLocators.PROCEED_BUTTON);
		if(isElememtPresent(PageObjects.IndividualOnboardingCommonLocators.TnC_TITLE)) {
			clickObject(PageObjects.IndividualOnboardingCommonLocators.TC_AGREE_BUTTON);
			//inputOTP(getOTP("6123451131"));
			Assert.assertTrue(isElememtPresent(PageObjects.IndividualOnboardingCommonLocators.OTP_TITLE));
		} else {
			log.info("Tnc Page not loaded");
			Assert.assertTrue(false);
		}
	}

//	@Test(priority = 4, enabled = true, groups = {"IndividualCurrentAccount"})
//	public void testInvalidOTP() {
//		typeInput(PageObjects.IndividualCurrentAccountLocators.OTP_VALUE, "1");
//	}
	
	
}
