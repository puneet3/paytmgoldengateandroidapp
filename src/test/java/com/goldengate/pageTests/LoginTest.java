package com.goldengate.pageTests;

import static org.testng.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.goldengate.base.BaseMethods;
import com.goldengate.common.Constants;
import com.goldengate.pageObjects.PageObjects;

public class LoginTest extends BaseMethods {

	public static final Logger log = Logger.getLogger(LoginTest.class);

	@Test(priority = 0, enabled = true, groups = {"Login Screen"})
	public void testMobileValidation() {
		waitForLoad(PageObjects.LoginLocators.LOGIN_MOBILE_NUMBER);
		typeInput(PageObjects.LoginLocators.LOGIN_MOBILE_NUMBER, getTestData().from(1, 1, 2));
		dismissKeypad();
		clickObject(PageObjects.LoginLocators.LOGIN_BUTTON);
		isElememtPresent(PageObjects.LoginLocators.MOBILE_VALIDATION_MESSAGE);
		String actualMobileValidation = getText(PageObjects.LoginLocators.MOBILE_VALIDATION_MESSAGE);
		assertEquals(actualMobileValidation, Constants.expected_mobile_validation);
		log.info("Actual mobile validation is showing " + actualMobileValidation + "Expected mobile validation is "
				+ Constants.expected_mobile_validation);

	}

	@Test(priority = 1, enabled = true, groups = {"Login Screen"})
	public void testPasswordValidation() {

		typeInput(PageObjects.LoginLocators.LOGIN_MOBILE_NUMBER, getTestData().from(0, 1, 0));
		typeInput(PageObjects.LoginLocators.LOGIN_PASSWORD, "");
		dismissKeypad();
		clickObject(PageObjects.LoginLocators.LOGIN_BUTTON);
		isElememtPresent(PageObjects.LoginLocators.PASSWORD_VALIDATION_MESSAGE);
		String actualPasswordValidation = getText(PageObjects.LoginLocators.PASSWORD_VALIDATION_MESSAGE);
		assertEquals(actualPasswordValidation, Constants.expected_password_validation);
		log.info("Actual password validation is showing " + actualPasswordValidation
				+ "Expected password validation is " + Constants.expected_password_validation);

	}

	@Test(priority = 2, enabled = true, groups = {"Login Screen"})
	public void testValidLogin() {
		login(getTestData().from(0, 1, 0), getTestData().from(0, 1, 1));
		allowPermission(PageObjects.PermissionPopupLocators.ALLOW_PERMISSION_POPUP_MESSAGE);
		allowPermission(PageObjects.PermissionPopupLocators.ALLOW_PERMISSION_POPUP_MSG);

		
		assertEquals(isElememtPresent(PageObjects.HomeScreenLocators.MY_WORK), true);
		signout();

	}

}
