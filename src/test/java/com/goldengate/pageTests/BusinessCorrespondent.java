package com.goldengate.pageTests;

import static org.testng.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.goldengate.base.BaseMethods;
import com.goldengate.common.Constants;
import com.goldengate.pageObjects.PageObjects;

public class BusinessCorrespondent extends BaseMethods {

	public static final Logger log = Logger.getLogger(BusinessCorrespondent.class);

	@Test(priority = 1, enabled = true, groups = { "BusinessCorrespondent" })

	public void nameOfBusinessOwnerValidation() throws Throwable {

		navigateToBascicDetails_BusinessCorrespondent();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.BASIC_DETAILS_NEXT_BUTTON);
		clickObject(PageObjects.BusinessCorrespondentLocators.BASIC_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.BusinessCorrespondentLocators.NAME_OF_BUSINESS_OWNER_VALIDATION),
				Constants.expected_business_owner_name_validation);
		log.info("TEST");

	}

	@Test(priority = 2, enabled = true, groups = { "BusinessCorrespondent" })

	public void nameOfBusinessValidation() throws Throwable {

		navigateToBascicDetails_BusinessCorrespondent();
		typeInput(PageObjects.BusinessCorrespondentLocators.NAME_OF_BUSINESS_OWNER, getTestData().from(3, 3, 2));
		dismissKeypad();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.BASIC_DETAILS_NEXT_BUTTON);
		clickObject(PageObjects.BusinessCorrespondentLocators.BASIC_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.QrMerchant100kLocators.NAME_OF_BUSINESS_VALIDATION),
				Constants.expected_business_name_validation);

	}

	@Test(priority = 3, enabled = true, groups = { "BusinessCorrespondent" })

	public void panValidation() throws Throwable {

		navigateToBascicDetails_BusinessCorrespondent();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.PAN_OPTIONAL);
		typeInput(PageObjects.BusinessCorrespondentLocators.PAN_OPTIONAL, "TEST");
		dismissKeypad();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.BASIC_DETAILS_NEXT_BUTTON);
		clickObject(PageObjects.BusinessCorrespondentLocators.BASIC_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.BusinessCorrespondentLocators.PAN_OPTIONAL_VALIDATION),
				Constants.expected_pan_regex);

	}

	@Test(priority = 4, enabled = true, groups = { "BusinessCorrespondent" })

	public void doingBusinessAsValidation() throws Throwable {

		navigateToAddressDetails_BusinessCorrespondent();
		verticalScroll(0.80, 0.20, PageObjects.QrMerchant100kLocators.ADDRESS_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.BusinessCorrespondentLocators.DOING_BUINESS_AS_VALIDATION),
				Constants.expected_doing_business_as_validation);

	}

	@Test(priority = 5, enabled = true, groups = { "BusinessCorrespondent" })

	public void houseValidation() throws Throwable {

		navigateToAddressDetails_BusinessCorrespondent();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDRESS_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.BusinessCorrespondentLocators.HOUSE_NO_VALIDATION),
				Constants.expected_house_no_validation);

	}

	@Test(priority = 6, enabled = true, groups = { "BusinessCorrespondent" })

	public void areaValidation() throws Throwable {

		navigateToAddressDetails_BusinessCorrespondent();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDRESS_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.BusinessCorrespondentLocators.AREA_VALIDATION),
				Constants.expected_area_validation);

	}

	@Test(priority = 7, enabled = true, groups = { "BusinessCorrespondent" })

	public void pincodeValidation() throws Throwable {

		navigateToAddressDetails_BusinessCorrespondent();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDRESS_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.BusinessCorrespondentLocators.PINCODE_VALIDATION),
				Constants.expected_pincode_validation);

	}

	@Test(priority = 8, enabled = true, groups = { "BusinessCorrespondent" })

	public void alternateNumberValidation() throws Throwable {

		navigateToAdditionalDetails1_BusinessCorrespondent();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
		verticalScroll(0.20, 0.80, PageObjects.BusinessCorrespondentLocators.ALTERNATE_NUMBER_OPTIONAL);
		typeInput(PageObjects.BusinessCorrespondentLocators.ALTERNATE_NUMBER_OPTIONAL, "456");
		dismissKeypad();
		assertEquals(getText(PageObjects.BusinessCorrespondentLocators.ALTERNATE_NUMBER_OPTIONAL_REGEX),
				Constants.expected_alternate_no_regex);

	}

	@Test(priority = 8, enabled = true, groups = { "BusinessCorrespondent" })

	public void languageValidation() throws Throwable {

		navigateToAdditionalDetails1_BusinessCorrespondent();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.BusinessCorrespondentLocators.LANGUAGE_PREFERENCES_VALIDATION),
				Constants.expected_language_validation);

	}

	@Test(priority = 8, enabled = true, groups = { "BusinessCorrespondent" })

	public void openingTimeValidation() throws Throwable {

		navigateToAdditionalDetails1_BusinessCorrespondent();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.BusinessCorrespondentLocators.OPENING_TIME_VALIDATION),
				Constants.expected_opening_time_validation);

	}

	@Test(priority = 8, enabled = true, groups = { "BusinessCorrespondent" })

	public void closingTimeValidation() throws Throwable {

		navigateToAdditionalDetails1_BusinessCorrespondent();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.BusinessCorrespondentLocators.CLOSING_TIME_VALIDATION),
				Constants.expected_closing_time_validation);

	}

	@Test(priority = 8, enabled = true, groups = { "BusinessCorrespondent" })

	public void emailIDValidation() throws Throwable {

		navigateToAdditionalDetails1_BusinessCorrespondent();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.BusinessCorrespondentLocators.EMAIL_ID_VALIDATION),
				Constants.expected_email_validation);

	}

	@Test(priority = 8, enabled = true, groups = { "BusinessCorrespondent" })

	public void gstnValidation() throws Throwable {

		navigateToAdditionalDetails1_BusinessCorrespondent();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.BusinessCorrespondentLocators.GSTN_OPTIONAL_VALIDATION),
				Constants.expected_gstn_optional_regex);

	}

	@Test(priority = 6, enabled = true, groups = { "BusinessCorrespondent" })

	public void testBusinessCorrespondent() throws Throwable {

		login(getTestData().from(0, 1, 0), getTestData().from(0, 1, 1));
		waitForLoad(6000);
		allowPermission(PageObjects.PermissionPopupLocators.ALLOW_PERMISSION_POPUP_MESSAGE);
		allowPermission(PageObjects.PermissionPopupLocators.ALLOW_PERMISSION_POPUP_MSG);
		waitForLoad(6000);
		isElememtPresent(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
		clickObject(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
		clickObject(PageObjects.IndividualOfferingsLocators.BUSINESS_CORRESPONDENT_LINK);
		typeInput(PageObjects.IndividualOnboardingCommonLocators.CUSTOMER_MOBILE_NUMBER, getTestData().from(3, 1, 2));
		clickObject(PageObjects.IndividualOnboardingCommonLocators.PROCEED_BUTTON);
		waitForLoad(12000);
		clickObject(PageObjects.IndividualOnboardingCommonLocators.TC_AGREE_BUTTON);
		inputOTP(getOTP(getTestData().from(3, 1, 2)));
		clickObject(PageObjects.IndividualOnboardingCommonLocators.VALIDATE_OTP_PROCEED_BUTTON);
		waitForLoad(30000);
		clickObject(PageObjects.IndividualOnboardingCommonLocators.LEAD_CREATD_LINK);
		waitForLoad(6000);
		clickObject(PageObjects.BusinessCorrespondentLocators.BCA_TYPE);
		clickObject(PageObjects.BusinessCorrespondentLocators.BCA_FIXED_TYPE);
		waitForLoad(2000);
		clickObject(PageObjects.BusinessCorrespondentLocators.BCA_SUBTYPE);
		clickObject(PageObjects.BusinessCorrespondentLocators.BCA_SUB_TYPE_TELECOM);
		typeInput(PageObjects.BusinessCorrespondentLocators.NAME_OF_BUSINESS_OWNER, getTestData().from(3, 3, 2));
		dismissKeypad();
		typeInput(PageObjects.BusinessCorrespondentLocators.NAME_OF_BUSINESS, getTestData().from(3, 2, 2));
		dismissKeypad();
		// typeInput(PageObjects.QrMerchant100kLocators.PAN_OPTIONAL,
		// getTestData().from(3, 4, 2));
		clickObject(PageObjects.BusinessCorrespondentLocators.BASIC_DETAILS_NEXT_BUTTON);
		typeInput(PageObjects.BusinessCorrespondentLocators.DOING_BUSINESS_AS, getTestData().from(3, 5, 2));
		dismissKeypad();
		typeInput(PageObjects.BusinessCorrespondentLocators.HOUSE_NO, getTestData().from(3, 6, 2));
		dismissKeypad();
		typeInput(PageObjects.BusinessCorrespondentLocators.STREET_NAME, getTestData().from(3, 7, 2));
		dismissKeypad();
		typeInput(PageObjects.BusinessCorrespondentLocators.AREA, getTestData().from(3, 8, 2));
		dismissKeypad();
		typeInput(PageObjects.BusinessCorrespondentLocators.PINCODE, getTestData().from(3, 9, 2));
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDRESS_DETAILS_NEXT_BUTTON);
		// typeInput(PageObjects.QrMerchant100kLocators.CITY_TOWN_DISTRICT,
		// getTestData().from(2, 11, 2));
		// dismissKeypad();
		// dismissKeypad();
		clickObject(PageObjects.BusinessCorrespondentLocators.ADDRESS_DETAILS_NEXT_BUTTON);
		typeInput(PageObjects.BusinessCorrespondentLocators.ALTERNATE_NUMBER_OPTIONAL, getTestData().from(3, 12, 2));
		dismissKeypad();
		clickObject(PageObjects.BusinessCorrespondentLocators.LANGUAGE_PREFERENCES);
		clickObject(PageObjects.BusinessCorrespondentLocators.LANGUAGE_ENGLISH);
		clickObject(PageObjects.BusinessCorrespondentLocators.OPENING_TIME);
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.SHOP_OPENING_TIME);
		clickObject(PageObjects.BusinessCorrespondentLocators.SHOP_OPENING_TIME);
		clickObject(PageObjects.BusinessCorrespondentLocators.CLOSING_TIME);
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.SHOP_CLOSING_TIME);
		clickObject(PageObjects.BusinessCorrespondentLocators.SHOP_CLOSING_TIME);
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.EMAIL_ID);
		typeInput(PageObjects.BusinessCorrespondentLocators.EMAIL_ID, getTestData().from(3, 15, 2));
		dismissKeypad();
		typeInput(PageObjects.BusinessCorrespondentLocators.GSTN_OPTIONAL, getTestData().from(3, 14, 2));
		dismissKeypad();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
		clickObject(PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
		waitForLoad(6000);
		clickObject(PageObjects.BusinessCorrespondentLocators.NO_OF_YEARS_IN_BUSINESS);
		clickObject(PageObjects.BusinessCorrespondentLocators.HOW_WIDE_STORE);
		clickObject(PageObjects.BusinessCorrespondentLocators.AVERAGE_DAILY_CUSTOMER_WALKINS);
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.DISTANCE_NEAREST_BRANCH);
		clickObject(PageObjects.BusinessCorrespondentLocators.DISTANCE_NEAREST_BRANCH);
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.TOTAL_NUMBER_OF_STAFF);
		clickObject(PageObjects.BusinessCorrespondentLocators.TOTAL_NUMBER_OF_STAFF);
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.EDUCATIONL_QUALIFICATION);
		clickObject(PageObjects.BusinessCorrespondentLocators.EDUCATIONL_QUALIFICATION);
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.DO_KYC_OF_ANOTHER_BANK);
		clickObject(PageObjects.BusinessCorrespondentLocators.DO_KYC_OF_ANOTHER_BANK);
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.AGE_OF_MANAGER);
		clickObject(PageObjects.BusinessCorrespondentLocators.AGE_OF_MANAGER);
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.HAVING_NETBANKING);
		clickObject(PageObjects.BusinessCorrespondentLocators.HAVING_NETBANKING);
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
		clickObject(PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
		clickObject(PageObjects.BusinessCorrespondentLocators.DECLARATION_CHECKBOX);
		verticalScroll(0.80, 0.20);
		clickObject(PageObjects.BusinessCorrespondentLocators.DECLARATION_CHECKBOX);
		clickObject(PageObjects.BusinessCorrespondentLocators.DECLARATION_NEXT_BUTTON);
		clickObject(PageObjects.BusinessCorrespondentLocators.PRODUCT_POSTER_PHOTO);
		captureImage();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.COMPLIANCE_POSTER_PHOTO);
		clickObject(PageObjects.BusinessCorrespondentLocators.COMPLIANCE_POSTER_PHOTO);
		captureImage();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.PPB_STICKER_PHOTO);
		clickObject(PageObjects.BusinessCorrespondentLocators.PPB_STICKER_PHOTO);
		captureImage();
		verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.SHOP_FRONT_PHOTO);
		clickObject(PageObjects.BusinessCorrespondentLocators.SHOP_FRONT_PHOTO);
		captureImage();
		clickObject(PageObjects.BusinessCorrespondentLocators.LEAD_SUBMIT_BUTTON);

	}

	public void navigateToBascicDetails_BusinessCorrespondent() {
		try {
		    login(getTestData().from(0, 1, 0), getTestData().from(0, 1, 1));
			waitForLoad(6000);
			allowPermission(PageObjects.PermissionPopupLocators.ALLOW_PERMISSION_POPUP_MESSAGE);
			allowPermission(PageObjects.PermissionPopupLocators.ALLOW_PERMISSION_POPUP_MSG);
			waitForLoad(6000);
			navigateBackToHome();
			isElememtPresent(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
			clickObject(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
			clickObject(PageObjects.IndividualOfferingsLocators.BUSINESS_CORRESPONDENT_LINK);
			typeInput(PageObjects.IndividualOnboardingCommonLocators.CUSTOMER_MOBILE_NUMBER,
					getTestData().from(3, 1, 2));
			clickObject(PageObjects.IndividualOnboardingCommonLocators.PROCEED_BUTTON);
			waitForLoad(12000);
			clickObject(PageObjects.IndividualOnboardingCommonLocators.TC_AGREE_BUTTON);
			inputOTP(getOTP(getTestData().from(3, 1, 2)));
			clickObject(PageObjects.IndividualOnboardingCommonLocators.VALIDATE_OTP_PROCEED_BUTTON);
			waitForLoad(30000);
			clickObject(PageObjects.IndividualOnboardingCommonLocators.LEAD_CREATD_LINK);
			waitForLoad(6000);
		} catch (Exception e) {
			log.info(e);

		}

	}

	public void navigateToAddressDetails_BusinessCorrespondent() throws Throwable {
		try {
			navigateToBascicDetails_BusinessCorrespondent();
			clickObject(PageObjects.BusinessCorrespondentLocators.BCA_TYPE);
			clickObject(PageObjects.BusinessCorrespondentLocators.BCA_FIXED_TYPE);
			waitForLoad(2000);
			clickObject(PageObjects.BusinessCorrespondentLocators.BCA_SUBTYPE);
			clickObject(PageObjects.BusinessCorrespondentLocators.BCA_SUB_TYPE_TELECOM);
			typeInput(PageObjects.BusinessCorrespondentLocators.NAME_OF_BUSINESS_OWNER, getTestData().from(3, 3, 2));
			dismissKeypad();
			typeInput(PageObjects.BusinessCorrespondentLocators.NAME_OF_BUSINESS, getTestData().from(3, 2, 2));
			dismissKeypad();
			// typeInput(PageObjects.QrMerchant100kLocators.PAN_OPTIONAL,
			// getTestData().from(3, 4, 2));
			clickObject(PageObjects.BusinessCorrespondentLocators.BASIC_DETAILS_NEXT_BUTTON);

		} catch (Exception e) {
			log.info(e);
		}

	}

	public void navigateToAdditionalDetails1_BusinessCorrespondent() throws Throwable {
		try {
			navigateToAddressDetails_BusinessCorrespondent();
			typeInput(PageObjects.BusinessCorrespondentLocators.DOING_BUSINESS_AS, getTestData().from(3, 5, 2));
			dismissKeypad();
			typeInput(PageObjects.BusinessCorrespondentLocators.HOUSE_NO, getTestData().from(3, 6, 2));
			dismissKeypad();
			typeInput(PageObjects.BusinessCorrespondentLocators.STREET_NAME, getTestData().from(3, 7, 2));
			dismissKeypad();
			typeInput(PageObjects.BusinessCorrespondentLocators.AREA, getTestData().from(3, 8, 2));
			dismissKeypad();
			typeInput(PageObjects.BusinessCorrespondentLocators.PINCODE, getTestData().from(3, 9, 2));
			verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDRESS_DETAILS_NEXT_BUTTON);
			// typeInput(PageObjects.QrMerchant100kLocators.CITY_TOWN_DISTRICT,
			// getTestData().from(2, 11, 2));
			// dismissKeypad();
			// dismissKeypad();
			clickObject(PageObjects.BusinessCorrespondentLocators.ADDRESS_DETAILS_NEXT_BUTTON);

		} catch (Exception e) {
			log.info(e);
		}

	}

	public void navigateToAdditionalDetails2_BusinessCorrespondent() throws Throwable {
		try {
			navigateToAdditionalDetails1_BusinessCorrespondent();
			typeInput(PageObjects.BusinessCorrespondentLocators.ALTERNATE_NUMBER_OPTIONAL,
					getTestData().from(3, 12, 2));
			dismissKeypad();
			clickObject(PageObjects.BusinessCorrespondentLocators.LANGUAGE_PREFERENCES);
			clickObject(PageObjects.BusinessCorrespondentLocators.LANGUAGE_ENGLISH);
			clickObject(PageObjects.BusinessCorrespondentLocators.OPENING_TIME);
			verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.SHOP_OPENING_TIME);
			clickObject(PageObjects.BusinessCorrespondentLocators.SHOP_OPENING_TIME);
			clickObject(PageObjects.BusinessCorrespondentLocators.CLOSING_TIME);
			verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.SHOP_CLOSING_TIME);
			clickObject(PageObjects.BusinessCorrespondentLocators.SHOP_CLOSING_TIME);
			verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.EMAIL_ID);
			typeInput(PageObjects.BusinessCorrespondentLocators.EMAIL_ID, getTestData().from(3, 15, 2));
			dismissKeypad();
			typeInput(PageObjects.BusinessCorrespondentLocators.GSTN_OPTIONAL, getTestData().from(3, 14, 2));
			dismissKeypad();
			verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
			clickObject(PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);

		} catch (Exception e) {
			log.info(e);
		}

	}

	public void navigatToAgentDeclaration_BusinessCorrespondent() throws Throwable {
		try {

			navigateToAdditionalDetails2_BusinessCorrespondent();
			clickObject(PageObjects.BusinessCorrespondentLocators.NO_OF_YEARS_IN_BUSINESS);
			clickObject(PageObjects.BusinessCorrespondentLocators.HOW_WIDE_STORE);
			clickObject(PageObjects.BusinessCorrespondentLocators.AVERAGE_DAILY_CUSTOMER_WALKINS);
			verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.DISTANCE_NEAREST_BRANCH);
			clickObject(PageObjects.BusinessCorrespondentLocators.DISTANCE_NEAREST_BRANCH);
			verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.TOTAL_NUMBER_OF_STAFF);
			clickObject(PageObjects.BusinessCorrespondentLocators.TOTAL_NUMBER_OF_STAFF);
			verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.EDUCATIONL_QUALIFICATION);
			clickObject(PageObjects.BusinessCorrespondentLocators.EDUCATIONL_QUALIFICATION);
			verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.DO_KYC_OF_ANOTHER_BANK);
			clickObject(PageObjects.BusinessCorrespondentLocators.DO_KYC_OF_ANOTHER_BANK);
			verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.AGE_OF_MANAGER);
			clickObject(PageObjects.BusinessCorrespondentLocators.AGE_OF_MANAGER);
			verticalScroll(0.80, 0.20, PageObjects.BusinessCorrespondentLocators.HAVING_NETBANKING);
			clickObject(PageObjects.BusinessCorrespondentLocators.HAVING_NETBANKING);
			clickObject(PageObjects.BusinessCorrespondentLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);

		} catch (Exception e) {
			log.info(e);
		}

	}

	public void navigateToCapturePhoto_BusinessCorrespondent() throws Throwable {
		try {

			navigatToAgentDeclaration_BusinessCorrespondent();
			clickObject(PageObjects.BusinessCorrespondentLocators.DECLARATION_CHECKBOX);
			verticalScroll(0.80, 0.20);
			clickObject(PageObjects.BusinessCorrespondentLocators.DECLARATION_CHECKBOX);
			clickObject(PageObjects.BusinessCorrespondentLocators.DECLARATION_NEXT_BUTTON);

		} catch (Exception e) {
			log.info(e);
		}

	}

}
