package com.goldengate.pageTests;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.goldengate.base.BaseMethods;
import com.goldengate.common.Constants;
import com.goldengate.pageObjects.PageObjects;

public class QRMerchant100k extends BaseMethods {

	public static final Logger log = Logger.getLogger(QRMerchant100k.class);

	@Test(priority = 1, enabled = true, groups = {"QRMerchant100k"})

	public void nameOfBusinessOwnerValidation() throws Throwable {

		navigateToBascicDetails_100k();
		verticalScroll(0.80, 0.20, PageObjects.QrMerchant100kLocators.BASIC_DETAILS_NEXT_BUTTON);
		clickObject(PageObjects.QrMerchant100kLocators.BASIC_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.QrMerchant100kLocators.NAME_OF_BUSINESS_OWNER_VALIDATION),
				Constants.expected_business_owner_name_validation);

	}

	@Test(priority = 2, enabled = true, groups = {"QRMerchant100k"})

	public void nameOfBusinessValidation() throws Throwable {

		navigateToBascicDetails_100k();
		verticalScroll(0.80, 0.20, PageObjects.QrMerchant100kLocators.BASIC_DETAILS_NEXT_BUTTON);
		clickObject(PageObjects.QrMerchant100kLocators.BASIC_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.QrMerchant100kLocators.NAME_OF_BUSINESS_VALIDATION),
				Constants.expected_business_name_validation);

	}

	@Test(priority = 3, enabled = true, groups = {"QRMerchant100k"})

	public void panValidation() throws Throwable {

		navigateToBascicDetails_100k();
		verticalScroll(0.80, 0.20, PageObjects.QrMerchant100kLocators.PAN_OPTIONAL);
		typeInput(PageObjects.QrMerchant100kLocators.PAN_OPTIONAL, "TEST");
		dismissKeypad();
		verticalScroll(0.80, 0.20, PageObjects.QrMerchant100kLocators.BASIC_DETAILS_NEXT_BUTTON);
		clickObject(PageObjects.QrMerchant100kLocators.BASIC_DETAILS_NEXT_BUTTON);
		assertEquals(getText(PageObjects.QrMerchant100kLocators.PAN_OPTIONAL_VALIDATION), Constants.expected_pan_regex);

	}

	@Test(priority = 4, enabled = true, groups = {"QRMerchant100k"})

	public void doingBusinessAsValidation() throws Throwable {

		navigateToAddressDetails_100k();
		assertEquals(getText(PageObjects.QrMerchant100kLocators.DOING_BUINESS_AS_VALIDATION),
				Constants.expected_doing_business_as_validation);

	}

	@Test(priority = 5, enabled = true, groups = {"QRMerchant100k"})

	public void houseValidation() throws Throwable {

		navigateToAddressDetails_100k();
		assertEquals(getText(PageObjects.QrMerchant100kLocators.HOUSE_NO_VALIDATION),
				Constants.expected_house_no_validation);

	}

	@Test(priority = 6, enabled = true, groups = {"QRMerchant100k"})
	public void test100k_FreshMerchant() throws Throwable, IOException {

		login(getTestData().from(0, 1, 0), getTestData().from(0, 1, 1));
		allowPermission(PageObjects.PermissionPopupLocators.ALLOW_PERMISSION_POPUP_MESSAGE);
		allowPermission(PageObjects.PermissionPopupLocators.ALLOW_PERMISSION_POPUP_MSG);
		isElememtPresent(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
		clickObject(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
		clickObject(PageObjects.IndividualOfferingsLocators.QR_MERCHANT_LINK);
		clickObject(PageObjects.QrMerchant100kLocators.ACCEPT_PAYMENT_IN_BANK_ACCOUNT);
		typeInput(PageObjects.IndividualOnboardingCommonLocators.CUSTOMER_MOBILE_NUMBER, getTestData().from(2, 1, 2));
		clickObject(PageObjects.IndividualOnboardingCommonLocators.PROCEED_BUTTON);
		clickObject(PageObjects.IndividualOnboardingCommonLocators.TC_AGREE_BUTTON);
		inputOTP(getOTP(getTestData().from(2, 1, 2)));
		clickObject(PageObjects.IndividualOnboardingCommonLocators.VALIDATE_OTP_PROCEED_BUTTON);
		clickObject(PageObjects.IndividualOnboardingCommonLocators.LEAD_CREATD_LINK);
		clickObject(PageObjects.QrMerchant100kLocators.CATEGORY);
		clickObject(PageObjects.QrMerchant100kLocators.AUTOMOBILES_AND_VEHICLES);
		clickObject(PageObjects.QrMerchant100kLocators.SUBCATEGORY);
		clickObject(PageObjects.QrMerchant100kLocators.AUTHORIZED_SERVICE_CENTRE);
		typeInput(PageObjects.QrMerchant100kLocators.NAME_OF_BUSINESS_OWNER, getTestData().from(2, 2, 2));
		dismissKeypad();
		typeInput(PageObjects.QrMerchant100kLocators.NAME_OF_BUSINESS, getTestData().from(2, 3, 2));
		dismissKeypad();
		// typeInput(PageObjects.QrMerchant100kLocators.PAN_OPTIONAL,
		// getTestData().from(2, 4, 2));
		clickObject(PageObjects.QrMerchant100kLocators.BASIC_DETAILS_NEXT_BUTTON);
		typeInput(PageObjects.QrMerchant100kLocators.DOING_BUSINESS_AS, getTestData().from(2, 5, 2));
		dismissKeypad();
		typeInput(PageObjects.QrMerchant100kLocators.HOUSE_NO, getTestData().from(2, 6, 2));
		dismissKeypad();
		typeInput(PageObjects.QrMerchant100kLocators.STREET_NAME, getTestData().from(2, 7, 2));
		dismissKeypad();
		typeInput(PageObjects.QrMerchant100kLocators.AREA, getTestData().from(2, 8, 2));
		dismissKeypad();
		typeInput(PageObjects.QrMerchant100kLocators.PINCODE, getTestData().from(2, 9, 2));
		verticalScroll(0.80, 0.20, PageObjects.QrMerchant100kLocators.ADDRESS_DETAILS_NEXT_BUTTON);
		// typeInput(PageObjects.QrMerchant100kLocators.CITY_TOWN_DISTRICT,
		// getTestData().from(2, 11, 2));
		// dismissKeypad();
		// dismissKeypad();
		System.out.println("hjsgduygdsahdhsahdjshadhsajjdhsjahjdxhsaj");
		clickObject(PageObjects.QrMerchant100kLocators.ADDRESS_DETAILS_NEXT_BUTTON);
		typeInput(PageObjects.QrMerchant100kLocators.ALTERNATE_NUMBER_OPTIONAL, getTestData().from(2, 12, 2));
		dismissKeypad();
		clickObject(PageObjects.QrMerchant100kLocators.LANGUAGE_PREFERENCES);
		clickObject(PageObjects.QrMerchant100kLocators.HINDI_LANGUAGE);
		typeInput(PageObjects.QrMerchant100kLocators.GSTN_OPTIONAL, getTestData().from(2, 14, 2));
		dismissKeypad();
		clickObject(PageObjects.QrMerchant100kLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
		typeInput(PageObjects.QrMerchant100kLocators.BANK_ACCOUNT_NO, getTestData().from(2, 15, 2));
		dismissKeypad();
		typeInput(PageObjects.QrMerchant100kLocators.IFSC_CODE, getTestData().from(2, 16, 2));
		dismissKeypad();
		clickObject(PageObjects.QrMerchant100kLocators.CONFIRM_BUTTON);
		clickObject(PageObjects.QrMerchant100kLocators.QR_STICKER1);
		captureImage();
		verticalScroll(0.80, 0.20, PageObjects.QrMerchant100kLocators.PAYTM_ACCEPTED_HERE1);
		clickObject(PageObjects.QrMerchant100kLocators.PAYTM_ACCEPTED_HERE1);
		captureImage();
		verticalScroll(0.80, 0.20, PageObjects.QrMerchant100kLocators.SHOP_FRONT_PHOTO);
		clickObject(PageObjects.QrMerchant100kLocators.SHOP_FRONT_PHOTO);
		captureImage();
		verticalScroll(0.80, 0.20, PageObjects.QrMerchant100kLocators.CANCEL_CHEQUE_PHOTO);
		clickObject(PageObjects.QrMerchant100kLocators.CANCEL_CHEQUE_PHOTO);
		captureImage();
		clickObject(PageObjects.QrMerchant100kLocators.LEAD_SUBMIT_BUTTON);

	}

	public void navigateToBascicDetails_100k() throws Throwable {

		try {
			login(getTestData().from(0, 1, 0), getTestData().from(0, 1, 1));
			allowPermission(PageObjects.PermissionPopupLocators.ALLOW_PERMISSION_POPUP_MESSAGE);
			waitForLoad(6);
			navigateBackToHome();

			isElememtPresent(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
			clickObject(PageObjects.HomeScreenLocators.INDIVIDUAL_OFFERINGS);
			clickObject(PageObjects.IndividualOfferingsLocators.QR_MERCHANT_LINK);
			clickObject(PageObjects.QrMerchant100kLocators.ACCEPT_PAYMENT_IN_BANK_ACCOUNT);
			typeInput(PageObjects.IndividualOnboardingCommonLocators.CUSTOMER_MOBILE_NUMBER,
					getTestData().from(2, 1, 2));
			clickObject(PageObjects.IndividualOnboardingCommonLocators.PROCEED_BUTTON);
			waitForLoad(3000);
			clickObject(PageObjects.IndividualOnboardingCommonLocators.TC_AGREE_BUTTON);
			inputOTP(getOTP(getTestData().from(2, 1, 2)));
			clickObject(PageObjects.IndividualOnboardingCommonLocators.VALIDATE_OTP_PROCEED_BUTTON);
			waitForLoad(15000);
			clickObject(PageObjects.IndividualOnboardingCommonLocators.LEAD_CREATD_LINK);
			waitForLoad(3000);
		} catch (Exception e) {
			log.info(e);
		}
	}

	public void navigateToAddressDetails_100k() throws Throwable {
		try {
			navigateToBascicDetails_100k();
			clickObject(PageObjects.QrMerchant100kLocators.CATEGORY);
			clickObject(PageObjects.QrMerchant100kLocators.AUTOMOBILES_AND_VEHICLES);
			clickObject(PageObjects.QrMerchant100kLocators.SUBCATEGORY);
			clickObject(PageObjects.QrMerchant100kLocators.AUTHORIZED_SERVICE_CENTRE);
			typeInput(PageObjects.QrMerchant100kLocators.NAME_OF_BUSINESS_OWNER, getTestData().from(2, 2, 2));
			dismissKeypad();
			typeInput(PageObjects.QrMerchant100kLocators.NAME_OF_BUSINESS, getTestData().from(2, 3, 2));
			dismissKeypad();
			// typeInput(PageObjects.QrMerchant100kLocators.PAN_OPTIONAL,
			// getTestData().from(2, 4, 2));
			clickObject(PageObjects.QrMerchant100kLocators.BASIC_DETAILS_NEXT_BUTTON);
			clearTextbox(PageObjects.QrMerchant100kLocators.DOING_BUSINESS_AS);
			verticalScroll(0.80, 0.20, PageObjects.QrMerchant100kLocators.ADDRESS_DETAILS_NEXT_BUTTON);

		} catch (Exception e) {
			log.info(e);
		}
	}

}
