package com.goldengate.pageTests;

import static org.junit.Assert.assertTrue;

import java.awt.print.Pageable;
import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.goldengate.base.BaseMethods;
import com.goldengate.common.Constants;
import com.goldengate.pageObjects.PageObjects;
import com.jcraft.jsch.JSchException;

import junit.framework.Assert;

public class BusinessOfferingsProprietorship extends BaseMethods{
	
	// Test Data --  Give details of a KYCed Applicant
	public static String customerMobileNumber = "6123451158";
	public static String otp = null;
	public static String businessPAN = "CRZPK1116H";
	public static String businessName = "PG BN Regression a";
	public static String businessDisplayName = "PG DN Regression a";
	public static String category = "BFSI";
	public static String subCategory = "Loans";
	public static String addressHouseNumber = "12345";
	public static String addressStreetName = "PG SN Regression a";
	public static String addressArea = "PG A Regression a";
	public static String addressPinCode = "201301";
	public static String gstin = "";
	public static String activityProofType = "GST certificate";
	
	@Test(priority = 0, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testValidNavigation() {
		waitForLoad(5000);
		login("6123451111", "agent123");
		waitForLoad(5000);
		if(navigateCustomerMobileScreen()) {
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_TITLE));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 1, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testValidateMobileNoNumber() {
		if(navigateCustomerMobileScreen()) {
			typeInput(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_MOBILE_NUMBER, "");
			clickObject(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_PROCEED_BUTTON);
			Assert.assertEquals(Constants.expected_mobile_validation_no_number, 
					getText(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_MOBILE_VALIDATION_MESSAGE));			
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 2, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testValidateMobileWrongNumber() {
		if(navigateCustomerMobileScreen()) {
			typeInput(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_MOBILE_NUMBER, "1234512345");
			clickObject(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_PROCEED_BUTTON);
			Assert.assertEquals(Constants.expected_mobile_validation_wrong_number, 
					getText(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_MOBILE_VALIDATION_MESSAGE));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}

	@Test(priority = 3, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testValidMobileSubmit() {
		if(submitMobileNumber()) {
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.OTP_TITLE));	
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 4, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testValidateInvalidOTP() {
		if(submitMobileNumber()) {
			inputOTP("123123");
			clickObject(PageObjects.BusinessOfferingsCommonLocators.OTP_PROCEED_BUTTON);
//			waitForLoad(5000);
			waitForLoad(PageObjects.ErrorPopupLocators.ERROR_POPUP_OK_BUTTON);
			String actualErrorPopupMessage = getText(PageObjects.ErrorPopupLocators.ERROR_POPUP_MESSAGE);
			Assert.assertTrue(actualErrorPopupMessage.contains(Constants.invalid_otp_message));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 5, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testValidateUsedOTP() throws JSchException, IOException {
		if(submitMobileNumber()) {
			submitValidOTP();
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_TITLE);
			submitMobileNumber();
//			waitForLoad(5000);
			inputOTP(otp);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.OTP_PROCEED_BUTTON);
//			waitForLoad(5000);
			waitForLoad(PageObjects.ErrorPopupLocators.ERROR_POPUP_OK_BUTTON);
			String actualErrorPopupMessage = getText(PageObjects.ErrorPopupLocators.ERROR_POPUP_MESSAGE);
			Assert.assertTrue(actualErrorPopupMessage.contains(Constants.usedOtpMessage));			
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}

	@Test(priority = 6, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testBusinessDetailsSolutionSelectionValidation() throws JSchException, IOException {
		if(submitValidOTP()) {
//			waitForLoad(5000);
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_TITLE);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PROCEED_BUTTON);
			Assert.assertEquals(Constants.expected_business_details_no_selection, 
					getText(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_RADIO_BUTTON_ERROR));			
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 7, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testBusinessDetailsProprietorNoValidation() throws JSchException, IOException {
		if(submitValidOTP()) {
//			waitForLoad(5000);
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_TITLE);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PROPRIETORSHIP_RADIO_BUTTON);
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PROPRIETOR_NO_RADIO_BUTTON);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PROCEED_BUTTON);
			waitForLoad(PageObjects.ErrorPopupLocators.ERROR_POPUP_OK_BUTTON);
			String actualErrorPopupMessage = getText(PageObjects.ErrorPopupLocators.ERROR_POPUP_MESSAGE);
			Assert.assertTrue(actualErrorPopupMessage.contains(Constants.expected_business_details_no_proprietor_selection));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 8, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testBusinessDetailsProprietorInvalidPANValidation() throws JSchException, IOException {
		if(submitValidOTP()) {
//			waitForLoad(5000);
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_TITLE);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PROPRIETORSHIP_RADIO_BUTTON);
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PROPRIETOR_YES_RADIO_BUTTON);
			typeInput(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_BUSINESS_PAN, "abc");
			dismissKeypad();
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PROCEED_BUTTON);
			String actualErrorMessage = getText(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_BUSINESS_PAN_ERROR);
			Assert.assertTrue(actualErrorMessage.contains(Constants.expected_business_valid_pan_validation));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}	

	@Test(priority = 9, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testCreateProprietorLead() throws JSchException, IOException {
		if(createProprietorLead()) {
//			waitForLoad(5000);
			Assert.assertEquals(getText(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS), " Lead Created");
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
		
	@Test(priority = 10, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testSelectBusinessProfileLeadCreated() throws JSchException, IOException {
		if(selectBusinessSolutionfromBO(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS)) {
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_TYPE));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}
	}
	
	@Test(priority = 11, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testBusinessDetailsCategoryValidation() throws JSchException, IOException {
		if(selectBusinessSolutionfromBO(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS)) {
//			waitForLoad(5000);
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_PROCEED_BUTTON);
			Assert.assertEquals(Constants.expected_category_validation_message, 
					getText(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_CATEGGORY_ERROR));			
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 12, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testBusinessDetailsSubCategoryValidation() throws JSchException, IOException {
		if(selectBusinessSolutionfromBO(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS)) {
//			waitForLoad(5000);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_CATEGORY);
			setRadioButtonValue(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_SELECT_DROPDOWN_VALUE, category);
//			waitForLoad(3000);
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_CATEGORY);
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_PROCEED_BUTTON);
			Assert.assertEquals(Constants.expected_sub_category_validation_message, 
					getText(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_SUB_CATEGOTY_ERROR));			
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}	

	@Test(priority = 13, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testBusinessDetailsDisplayNameValidation() throws JSchException, IOException {
		if(selectBusinessSolutionfromBO(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS)) {
//			waitForLoad(5000);
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_PROCEED_BUTTON);
			Assert.assertEquals(Constants.expected_display_name_validation_message, 
					getText(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_DISPLAY_NAME_ERROR));			
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 14, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testSubmitValidBusinessDetails() throws JSchException, IOException {
		if(submitValidBusinessDetails()) {
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_HOUSE_NUMBER));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 15, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testAddressHouseNumberValidationNoInput() throws JSchException, IOException {
		if(submitValidBusinessDetails()) {
			clickObject(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_NEXT_BUTTON);
			Assert.assertEquals(Constants.expected_house_no_validation, 
					getText(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_HOUSE_NUMBER_ERROR));				
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}

	@Test(priority = 16, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testAddressHouseNumberValidationWrongInput() throws JSchException, IOException {
		if(submitValidBusinessDetails()) {
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_HOUSE_NUMBER, "12%");
			dismissKeypad();
			clickObject(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_NEXT_BUTTON);
			Assert.assertEquals(Constants.expected_house_no_wrong_input_validation, 
					getText(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_HOUSE_NUMBER_ERROR));				
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}

	@Test(priority = 17, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testAddressStreetNameValidationWrongInput() throws JSchException, IOException {
		if(submitValidBusinessDetails()) {
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_STREET_NAME, "SN%");
			dismissKeypad();
			clickObject(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_NEXT_BUTTON);
			Assert.assertEquals(Constants.expected_street_name_wrong_input_validation, 
					getText(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_STREET_NAME_ERROR));				
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 18, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testAddressAreaValidationNoInput() throws JSchException, IOException {
		if(submitValidBusinessDetails()) {
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_HOUSE_NUMBER, addressHouseNumber);
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_STREET_NAME, addressStreetName);
			dismissKeypad();
			clickObject(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_NEXT_BUTTON);
			Assert.assertEquals(Constants.expected_area_validation, 
					getText(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_AREA_ERROR));				
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}

	@Test(priority = 19, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testAddressAreaValidationWrongInput() throws JSchException, IOException {
		if(submitValidBusinessDetails()) {
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_HOUSE_NUMBER, addressHouseNumber);
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_STREET_NAME, addressStreetName);
			dismissKeypad();
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_AREA, "PG AN%");
			dismissKeypad();
			clickObject(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_NEXT_BUTTON);
			Assert.assertEquals(Constants.expected_area_wrong_input_validation, 
					getText(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_AREA_ERROR));				
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 20, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testAddressPINCodeValidationNoInput() throws JSchException, IOException {
		if(submitValidBusinessDetails()) {
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_HOUSE_NUMBER, addressHouseNumber);
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_STREET_NAME, addressStreetName);
			dismissKeypad();
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_AREA, addressArea);
			dismissKeypad();
			clickObject(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_NEXT_BUTTON);
			Assert.assertEquals(Constants.expected_pincode_validation, 
					getText(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_PIN_CODE_ERROR));				
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 21, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testAddressPINCodeValidationIncompleteInput() throws JSchException, IOException {
		if(submitValidBusinessDetails()) {
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_HOUSE_NUMBER, addressHouseNumber);
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_STREET_NAME, addressStreetName);
			dismissKeypad();
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_AREA, addressArea);
			dismissKeypad();
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_PIN_CODE, addressHouseNumber);
			dismissKeypad();
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_NEXT_BUTTON);
			Assert.assertEquals(Constants.expected_pincode_invalid_message, 
					getText(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_PIN_CODE_ERROR));				
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 22, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testAddressPINCodeValidationInvalidInput() throws JSchException, IOException {
		if(submitValidBusinessDetails()) {
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_PIN_CODE, "999999");
//			waitForLoad(3000);
			waitForLoad(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_PIN_CODE);
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_NEXT_BUTTON);
			String actualErrorMessage = getText(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_PIN_CODE_ERROR);
			Assert.assertTrue(actualErrorMessage.contains(Constants.expected_pincode_failed_fetch_message));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 23, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testSubmitValidAddress() throws JSchException, IOException {
		if(submitValidAddress()) {
			//Add Addtional Details title locator
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.ADDITIONAL_DETAILS_GSTIN));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 24, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testAdditionalDetailsGSTINValidation() throws JSchException, IOException {
		if(submitValidAddress()) {
			typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDITIONAL_DETAILS_GSTIN, "INVALID");
			clickObject(PageObjects.BusinessOfferingsCommonLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
			
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.ADDITIONAL_DETAILS_GSTIN));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}			
	}
	
	@Test(priority = 25, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testSubmitValidAdditionalDetails() throws JSchException, IOException {
		if(submitValidAdditionalDetails()) {
			// Add Submit Business Document title locator
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_PHOTO_PLACE_BUSINESS_LATER));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}	

	@Test(priority = 26, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testSubmitDocumentNOSelectionValidationPlacePhoto() throws JSchException, IOException {
		if(submitValidAdditionalDetails()) {
//			waitForLoad(5000);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_SUBMIT_BUTTON);
			String actualErrorMessage = getText(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_PHOTO_PLACE_BUSINESS_ERROR_NO_SELECTION);
			Assert.assertTrue(actualErrorMessage.contains(Constants.expected_submit_document_no_selection));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}

	@Test(priority = 27, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testSubmitDocumentNOSelectionValidationActivityProof1() throws JSchException, IOException {
		if(submitValidAdditionalDetails()) {
//			waitForLoad(5000);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_SUBMIT_BUTTON);
			String actualErrorMessage = getText(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_ACTIVITY_PROOF_1_ERROR_NO_SELECTION);
			Assert.assertTrue(actualErrorMessage.contains(Constants.expected_submit_document_no_selection));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 28, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testSubmitDocumentShopPhotoValidation() throws JSchException, IOException {
		if(submitValidAdditionalDetails()) {
//			waitForLoad(5000);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_PHOTO_PLACE_BUSINESS_NOW);
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_SUBMIT_BUTTON);
			String actualErrorMessage = getText(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_PHOTO_PLACE_BUSINESS_ERROR_NO_PHOTO);
			Assert.assertTrue(actualErrorMessage.contains(Constants.expected_submit_document_no_photo));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 29, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testSubmitDocumentActivityProofDocumentSelectValidation() throws JSchException, IOException {
		if(submitValidAdditionalDetails()) {
//			waitForLoad(5000);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_ACTIVITY_PROOF_1_NOW);
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_SUBMIT_BUTTON);
			String actualErrorMessage = getText(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_ACTIVITTY_PROOF_1_DROPDOWN_ERROR);
			Assert.assertTrue(actualErrorMessage.contains(Constants.expected_submit_document_no_selection));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 30, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testSubmitDocumentActivityProofPhotoValidation() throws JSchException, IOException {
		if(submitValidAdditionalDetails()) {
//			waitForLoad(5000);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_ACTIVITY_PROOF_1_NOW);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_ACTIVITTY_PROOF_1_DROPDOWN);
			setRadioButtonValue(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_SELECT_DROPDOWN_VALUE, activityProofType);
			verticalScroll(0.8, 0.2);
			clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_SUBMIT_BUTTON);
			String actualErrorMessage = getText(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_ACTIVITY_PROOF_1_ERROR_NO_PHOTO);
			Assert.assertTrue(actualErrorMessage.contains(Constants.expected_submit_document_no_photo));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
	}
	
	@Test(priority = 31, enabled = true, groups = {"BusinessOfferingsProprietorship"})
	public void testSubmitValidDocuments() throws JSchException, IOException {
		if(submitValidDocuments()) {
//			waitForLoad(5000);
			// add list of docs title locator
			Assert.assertTrue(isElememtPresent(PageObjects.BusinessOfferingsCommonLocators.LIST_OF_DOCUMENTS_TO_UPLOAD_PROCEED_BUTTON));
		} else {
			log.info("Test failed because of Failed test Steps");
			Assert.assertTrue(false);
		}		
		
	}
	
	// Helper Functions | Test Steps
	public boolean navigateCustomerMobileScreen() {
		navigateBackToHome();
		clickObject(PageObjects.HomeScreenLocators.BUSINESS_OFFERINGS);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_TITLE);
		return true;
	}
	
	public boolean submitMobileNumber() {
		navigateCustomerMobileScreen();
		typeInput(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_MOBILE_NUMBER, customerMobileNumber);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.APPLICANT_MOBILE_NUMBER_PROCEED_BUTTON);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.OTP_TITLE);
		return true;
	}
	
	public boolean submitValidOTP() throws JSchException, IOException {
		submitMobileNumber();
//		waitForLoad(5000);
		otp = getOTP(customerMobileNumber);
		inputOTP(otp);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.OTP_PROCEED_BUTTON);
//		waitForLoad(5000);
		return true;
	}
	
	public boolean createProprietorLead() throws JSchException, IOException {
		submitValidOTP();
//		waitForLoad(5000);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_TITLE);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PROPRIETORSHIP_RADIO_BUTTON);
		verticalScroll(0.8, 0.2);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PROPRIETOR_YES_RADIO_BUTTON);
		typeInput(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_BUSINESS_PAN, businessPAN);
		dismissKeypad();
		verticalScroll(0.8, 0.2);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.PROVIDE_BUSINESS_DETAILS_PROCEED_BUTTON);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.CONFIRM_BUSINESS_DETAILS_TITLE);
		typeInput(PageObjects.BusinessOfferingsCommonLocators.CONFIRM_BUSINESS_DETAILS_BUSINESS_NAME, businessName);
		dismissKeypad();
		clickObject(PageObjects.BusinessOfferingsCommonLocators.CONFIRM_BUSINESS_DETAILS_CONFIRM_BUTTON);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_TITLE);
		return true;
	}
	public boolean reachBusinesProfileScreenFromHome() {
		
		return true;
	}
	
	public boolean selectBusinessSolutionfromBO(By businessSolution) throws JSchException, IOException {
		submitValidOTP();
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_TITLE);
		clickObject(businessSolution);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_TITLE);
		return true;
	}
	
	public boolean submitValidBusinessDetails() throws JSchException, IOException {
		selectBusinessSolutionfromBO(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_PROFILE_COMPANY_ONBOARD_STATUS);
//		waitForLoad(5000);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_CATEGORY);
		setRadioButtonValue(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_SELECT_DROPDOWN_VALUE, category);
//		waitForLoad(3000);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_CATEGORY);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_SUB_CATEGORY);
		setRadioButtonValue(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_SELECT_DROPDOWN_VALUE, subCategory);
//		waitForLoad(3000);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_BUSINESS_SUB_CATEGORY);
		verticalScroll(0.8, 0.2);
		typeInput(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_DISPLAY_NAME, businessDisplayName);
		dismissKeypad();
		verticalScroll(0.8, 0.2);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.BUSINESS_DETAILS_PROCEED_BUTTON);
		//Add Address title locator
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_HOUSE_NUMBER);
		return true;
	}
	
	public boolean submitValidAddress() throws JSchException, IOException {
		submitValidBusinessDetails();
//		waitForLoad(5000);
		typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_HOUSE_NUMBER, addressHouseNumber);
		typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_STREET_NAME, addressStreetName);
		dismissKeypad();
		typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_AREA, addressArea);
		dismissKeypad();
		typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_PIN_CODE, addressPinCode);
//		waitForLoad(5000);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_PIN_CODE);
		verticalScroll(0.8, 0.2);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.ADDRESS_NEXT_BUTTON);
		//Add title Page Locator
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.ADDITIONAL_DETAILS_GSTIN);
		return true;
	}
	
	public boolean submitValidAdditionalDetails() throws JSchException, IOException {
		submitValidAddress();
		typeInput(PageObjects.BusinessOfferingsCommonLocators.ADDITIONAL_DETAILS_GSTIN, gstin);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.ADDITIONAL_DETAILS_NEXT_BUTTON);
		// add submit document title locator
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_PHOTO_PLACE_BUSINESS_NOW);
		return true;
	}
	
	public boolean submitValidDocuments() throws JSchException, IOException {
		submitValidAdditionalDetails();
//		waitForLoad(5000);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_PHOTO_PLACE_BUSINESS_NOW);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_PHOTO_PLACE_BUSINESS_CAMERA);
		captureImage();
//		waitForLoad(3000);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_ACTIVITY_PROOF_1_NOW);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_ACTIVITY_PROOF_1_NOW);
		verticalScroll(0.8, 0.2);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_ACTIVITTY_PROOF_1_DROPDOWN);
		setRadioButtonValue(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_SELECT_DROPDOWN_VALUE, activityProofType);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_ACTIVITY_PROOF_1_CAMERA);
		captureImage();
//		waitForLoad(3000);
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_SUBMIT_BUTTON);
		clickObject(PageObjects.BusinessOfferingsCommonLocators.SUBMIT_BUSINESS_DOCUMENT_SUBMIT_BUTTON);
		//Add list of documents title locator
		waitForLoad(PageObjects.BusinessOfferingsCommonLocators.LIST_OF_DOCUMENTS_TO_UPLOAD_PROCEED_BUTTON);
		return true;
	}
}
